# -*- coding: utf-8 -*- 
'''
Created on 2017年6月10日

@author: siao
'''
import json

import requests
import logging
def vaildationCaptch(filepath ,strCaptch):
    
#     filepath = r"D:\tmp\google_vision.count"
    file = open(filepath,"r")
    count = file.read()
    file.close()
    logging.info("google.vision count:"+count) 
    if(int(count) >= 1000):
         
        logging.info("google vision alerady 1000 count,current use need pay 1.5USD so break")
        return None
    input = open(r'D:\tmp\vision.json', 'w')
    jsonObj = {"requests":[
                           {
                            "image":{"content":strCaptch},
                            "features" :[
                                         {"maxResults":5,"type":"TEXT_DETECTION"}
                                         ],
                            "imageContext":{"languageHints":["en"]}
                            }
            ]}
    input.write(json.dumps(jsonObj))
    input.close()
    output = open(r'D:\tmp\vision.json', 'rb')
    data = output.read()
    output.close()
    r = requests.post("https://vision.googleapis.com/v1/images:annotate?key=AIzaSyDfjS9nh9Jx7dSgNCY_sw0pUcxDQL-g6hQ", 
                      data=data,
                      headers={'content-type': 'application/json'})
    
    #add one count 
    file = open(filepath,"w")
    file.write(str(int(count)+1))
    file.close()
    return r.json()['responses'][0]['textAnnotations'][0]['description']

def subBase64Content(strCaptch):
    if(strCaptch == None):
        return None
    logging.info(strCaptch)
    strCaptch = strCaptch.split(",")[1]
    return strCaptch 

def trimSpace(result):
    resultArray = result.replace("\n", "").split(" ")
    allStr = ""
    logging.info(resultArray)
    for tmp in resultArray:
        allStr += tmp
    return allStr

def is_alphabet(uchar):         
    if (u'\u0041' <= uchar<=u'\u005a') or (u'\u0061' <= uchar<=u'\u007a'):
        return True
    else:
        return False
    
if __name__ == '__main__':
#     testStr = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFsAAAAYCAYAAACV+oFbAAAFhUlEQVR42u1Yf2wTVRx/7+56Lf11bdcN2nVjtGNjDtbBEJQfMvkxEJyRoGJINErARKeEmMyYyB8mmOCviDGaaDROoigQdagYARHYCENgdmww9sP9Yj/Kuv5mvWt7d71nj2RacGPdujFj+kle3r3PvXt99+n3fd73HfSXZ+wCSdwVYEkJkmInxU4iMRD/h5eo8Qi6r3tRHs0BsqIIP5UUOw68V0MXOoNINdL9LXOlDfOmS/xD7UAE4mV1XHGdD2TLcMCqJChQ7cL0D+ihKyn2KKjqDlkDLFDEcjgCyMcKWvF6jYVsjVY3xWYFhD1xni9leCArz8GPbjaBHhKDQtJG4sQPT6bsGy7aDzQy61fMlNaszCIdQ/xWW2RVVFnsq3uxw6ZpMDSV865zIDWL0N/7X7oChEwqLDQhYrcP8gougqB4nSqFbMo0gnUHedIZRqTImZUEQxKJR9m3V0NZh5qYklw90bx7pap6iK92IX29D2V/sgA/aJqGTanQvjAk3q/jitwhoBxggCZPB3pXz8Q6nsoD1xIWOxBB+DedgdkXBsLZi9KkbYtTZNfXpBOOH3uDGZXXmPmFOmnnzrlUgw6AhMQ+0R4y7K2hHzOp8Z5PSzWHZTF/XqVdsFAkvLFEh3mmejVqpIjft05yalcNP9+ggE1lVrxtwmxEicPI02ZlizMoKHcVaC6JXL2Hoy66WNMrVur0slRZwpuTOyyQe87SGxUSQH+0QV0pu22V+Dgg10pA4L9kge1eoC81w84J9+yrfpYyyfGbG9X+Djqr0RtOe6NIV6MjIZfopEM8wsp+8m8MsUC+d72qYrqCGNYmvBxQjjZWBwPkZjlgRrpf2SmkH+1FltHGWW2CHY/PwnrvHCBIuXgG5pvwQ02tm51hpgjfyxc9S+00r3rdqq2dCKFFvPiLf32PT8goX648tMhIuodduhLA+FmkbqGRYmS7g/h2G7fBxUPJSH16GKD0hP4pbX5osDlRzgANNbF8NCLRaL5Nc0g2KdmInY5QjV7OtCxN1lbVH5p9wRu+PhH2UX78RnFDP1fwXJHi+0dzpd0j9dtoxNpPDAgLPu8S8t7Kx2uH61PRFbE4Q1BnpwWZnho+EHbkYy078kHLUHtbVaS40QPkB1fDIySBxb3nHPqTz8zRYPZY7kyfoGtwIV2sh48rsnmE8A8XaY+X5ama9yzUnvyiNVCYqNCf2Zg5Z7rD95WYpVXbFsib79RXPLRkyOH1Xx1C4XmvoBnOPr7sFpauSoOXCig4GO8cWv3AlE3BvrFmUlddQurKTNgVy/3ciWYlbCOXvZxaK8VoMd0T2xYVQatJLChmKYlkHhW24MNaGeYuNJB9h1vCmbeXAIduWYUvWfBzvADxnfVCaazgYlq4tZZ/RCcF/t35RG3c7+WG6iCPZBY1HBjr/K+4kGmJAXfGcrYBIfNBE9afkI20DrJUgVbqiOUsSon7XH9YvyZd5hiP2Mfa2VwOCaQ7CFLfPju45V8RgQF+nUX/biz30HTosPnh2QPdwvLtfwibjXLBGY0cwR5CaWYF1vdBAfxNiaNIvHOweYQUsc7VwLjTyWeP8Sv6aKRr9iLL8ye5tbdaLdLfkwID4xb7zcs+azTNy1BJIGNneGX5XOqKyFv1EtfHTYMLuxiubftsVdtYxX6nRH06Wp0e63Ov5eBX5iihb38PskbTRUpBQOaZTKz6BTPWOtaje5NH0Iv1/TOgM95nKtYSVZN2XH91nqZeTKtv58XNMVqOTkV+u8mI9W4ygmhahic0Tr0bZBrk0JmlBMHJmmvye3YU0QMa6Q4DyqyOP6qTYo8TF11QhxDCzBRwJ8WeZPzeD4xiXWKAfUmxJxmNXmTUyuCNOTo4qd9biKTUAHxXgh25G7/zF8dKVTsaSOzCAAAAAElFTkSuQmCC"
#     testStr = subBase64Content(testStr)
#     result = trimSpace(vaildationCaptch(r"D:\tmp\google_vision.count",testStr))
    result = trimSpace("K ZCTI\n")
    for test in result:
        if test != "":
            print test
            check = not is_alphabet(test)
            print check
            if(check):
                print "check fail restart"
                break
    