# coding=utf-8
'''
Created on 2017年7月3日

@author: master
'''
from PIL.Image import Image
import os
import time

from PTC.downloadImage import  get_captcha, \
    validation_captcha
from PTC.gen_captcha import gen_captcha_text_and_image
from PTC.goldenClix.goldcClixTool import initBrowser
from PTC.mergePng import create_captcha
from PTC.train_captcha import convert2gray, crack_captcha, \
    crack_captcha_cnn, MAX_CAPTCHA, CHAR_SET_LEN, X, keep_prob, vec2text
import numpy as np
import tensorflow as tf


def create_image():
    correct_text,image = gen_captcha_text_and_image(1)
    captcha_image = convert2gray(image)
    captcha_image = captcha_image.flatten() / 255
    return correct_text,captcha_image
# text, image = gen_captcha_text_and_image(1)
# text,image = create_captcha()
successCount = 0
failCount = 0
output = crack_captcha_cnn()
saver = tf.train.Saver()
foldername=time.strftime("%Y%m%d")
if (not os.path.exists('/home/master/'+foldername)):
    os.mkdir("/home/master/"+foldername)
with tf.Session() as sess:
    saver.restore(sess, tf.train.latest_checkpoint('./model'))
    print "checkpoint:",tf.train.latest_checkpoint('./model')
    predict = tf.argmax(tf.reshape(output, [-1, MAX_CAPTCHA, CHAR_SET_LEN]), 2)
    browser = initBrowser("webdriver","http://www.goldenclix.com/index.php?view=login")
    for step in range(0,500,1):
        image = get_captcha(browser)
        validation_image = np.array(image) 
        captcha_image = convert2gray(validation_image)
        captcha_image = captcha_image.flatten() / 255
        text_list = sess.run(predict, feed_dict={X: [captcha_image], keep_prob: 1})
        text = text_list[0].tolist()  #應該是機率最高的那一組
        vector = np.zeros(MAX_CAPTCHA*CHAR_SET_LEN)
        i = 0
        for n in text:
                vector[i*CHAR_SET_LEN + n] = 1  #ex:ABCDE vector[1*26+65] vector[2*26+66] vector[3*26+67] ...
                i += 1
        predict_text =  vec2text(vector)
        result = validation_captcha(browser,predict_text)
        if(result):
            successCount += 1
            image.convert('RGB').save("/home/master/success_image/"+predict_text+".png")
            print "[SUCCESS]predict_text:",predict_text," step:",step
        else:
            failCount += 1
            image.convert('RGB').save("/home/master/"+foldername+"/"+predict_text+".png")
            print "[ERROR]predict_text:",predict_text," step:",step
#         print("正确: {}  预测: {}".format(correct_text, predict_text))
#     predict_text = crack_captcha(image)
    print "successCount:",str(successCount)," failCount:",str(failCount)


