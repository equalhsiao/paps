# -*- coding: utf-8 -*- 
'''
Created on 2017年7月25日

@author: siao
'''
from PIL import Image
from numpy.random.mtrand import randint
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time

from PTC.goldenClix.goldcClixTool import initBrowser, get_captcha, login, \
    autoClickAD, cutImageValidation


def initBrowserTest():
    initBrowser("http://www.goldenclix.com/index.php?view=login")
    time.sleep(randint(5,10))
    image1 = get_captcha()
    print "origin size:",image1.size
    img = image1.resize((180,40), Image.ANTIALIAS)
    print "img.size:",img.size
def loginTest():
    profile = webdriver.FirefoxProfile()
    browser = webdriver.Firefox(executable_path=r'/home/master/geckodriver',firefox_profile=profile)
    browser.get("http://www.goldenclix.com/index.php?view=login")
    time.sleep(15)
    browser.find_element_by_id("captcha_login").click()
    browser.close()
    for window in browser.window_handles:
        browser.switch_to_window(window)
    result = login(browser,"test")
    return browser
    print result

    
if __name__ == "__main__":
#      initBrowserTest()
    browser = loginTest()
    time.sleep(20)
    autoClickAD(browser)
#     cutImageValidation()
    