#coding=utf-8
'''
Created on 2017年7月17日

@author: master
'''
# -*- coding: utf-8 -*- 
'''
Created on 2017年6月20日

@author: siao
'''

from PIL import Image
import os
from random import randint
import numpy as np
from PTC.optTool import cleanbackgroup


def mergei(files):
    """
    横向拼接
    :param files:需要拼接的文件,list
#     :param output_file: 拼接完成后的输出文件
    :return：生成拼接后的新的图片
    """
    tot = len(files)
    img = Image.open(files[0])
    w, h = img.size[0], img.size[1]
    merge_img = Image.new('RGB', (w * tot, h), 0xffffff)
    i = 0
    for f in files:
#         print(f)
        img = Image.open(f)
        merge_img.paste(img, (i, 0))
        i += w
#     merge_img.show()
#     merge_img.save(output_file)
    return merge_img
def mergej(files, output_file):
    """
    纵向拼接
    :param files:需要拼接的文件,list
    :param output_file: 拼接完成后的输出文件
    :return：生成拼接后的新的图片
    """
    tot = len(files)
    img = Image.open(files[0])
    w, h = img.size[0], img.size[1]
    merge_img = Image.new('RGB', (w, h * tot), 0xffffff)
    j = 0
    for f in files:
        print(f)
        img = Image.open(f)
        merge_img.paste(img, (0, j))
        j += h
    merge_img.save(output_file)
def listFiles(path):
    """
    枚举某个路径下的全部文件名，返回list
    :param path 路径
    """
    files = os.listdir(path)
    return files
# if __name__=='__main__':
#     files = listFiles(r'D:\split_text')
#     print(len(files))
#     print str(files)
#     os.chdir(r'D:\tmp\image_tmp')

def create_captcha():
    text = ""
    filelist = [];
    for _step in range(0,5,1):
        lucknum = chr(randint(0,25)+65)
        text += lucknum
        files = listFiles(r'./split_text/'+str(lucknum));
        luckseq = randint(0,len(files)-1)
        filelist.append("./split_text/"+str(lucknum)+"/"+files[luckseq])
#         filelist
    image = mergei(filelist)
    width,height = image.size
    try:
        text,image = cleanbackgroup(width,height,image)
    except:
        pass
#         image.show()
#     image.shape =2
    image = np.array(image)
    return text,image
#     mergej(files, "merge.tif")