# coding=utf-8
'''
Created on 2017年7月30日

@author: master
'''
import ConfigParser
from PIL import Image
from datetime import timedelta
import datetime
import getpass
import json
import logging
import os
from random import randint
import requests
from selenium import webdriver
from selenium.common.exceptions import WebDriverException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import signal
from subprocess import Popen, PIPE
from subprocess import call
import subprocess
import sys
import time

from PTC.flow import indexClixFlow
from PTC.train_captcha import convert2gray, \
    crack_captcha_cnn, MAX_CAPTCHA, CHAR_SET_LEN, X, keep_prob, vec2text
import numpy as np
import tensorflow as tf


sys.path.append("/home/"+getpass.getuser()+"/workspace/PAPS")





    
class tools(object):
    logger = logging.basicConfig()
    output = None
    saver = None
    def __init__(self):
        self.output = crack_captcha_cnn()
        self.saver = tf.train.Saver()
        self.logger = logging.getLogger("tools")
        
    waitTime = 9 * 60 * 1000
    def refresh(self,browser):
        browser.execute_script("javascript:resetSessionLife();")
        email = browser.find_element_by_id("mailAddress").get_attribute('value')
        createTime = int(datetime.datetime.now().strftime("%s")) * 1000
        return email,createTime
    def validationEmail(self,createTime):
        nowTime = int(datetime.datetime.now().strftime("%s")) * 1000
        if(nowTime >= createTime + self.waitTime):
            return False
        else:
            return True
    #https://10minutemail.net/?lang=zh-tw
    #https://10minutemail.com/10MinuteMail/index.html
    def createEmail(self,webType,url,elementID):
            browser = self.initBrowser(webType, url, None,None)
            email = browser.find_element_by_id(elementID).get_attribute('value')
            createTime = int(datetime.datetime.now().strftime("%s")) * 1000 
            return browser,email,createTime
        
    def getIP(self):
        p = Popen(['wget', "-q","-O-","ifconfig.co"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, err = p.communicate(b"input data that is passed to subprocess' stdin")
        rc = p.returncode
        return output.strip()
    def getProxyIP(self,c):
        connection = c.cursor()
        rows = connection.execute("select IP,PORT from proxyserver where IP Not in ( select IP from userinfo) and  ANONYMITY = '普通匿名' and AVAILABILITY < 70.0 and RESPONSESPEED < 50 and status is Null or status =='1'  and proxytype = 'HTTPS' ORDER BY RANDOM() LIMIT 1")
        row =rows.fetchone()
        ip = row[0]
        port = row[1]
        print "get proxy ip:",ip,"get port:",port
        return ip+":"+port
    def testWebIP(self,proxy):
        browser = self.initBrowser("phantomjs","http://www.whatismyip.com.tw/", proxy, None)
        try:
            print browser.page_source
        except Exception as e:
            print str(e)
        browser.quit()
        
    def initBrowser(self, webType, url,proxy,secondurl="https://www.indexclix.com/?ref=equalhsiao"):
#         config = ConfigParser.ConfigParser()
        with open('./../../../DeepLearning.properties') as fp:
            config = ConfigParser.ConfigParser()
            config.readfp(fp)
            fp.close()
#             sections = config.sections()
#         config.read("./../../../DeepLearning.properties")
        if(webType == "webdriver"):
            profile = webdriver.FirefoxProfile()
            if not proxy is None:
                print "tools debug proxy:",proxy
                PROXY = proxy
                proxy = Proxy({"httpProxy":PROXY,
                               "ftpProxy":PROXY,
                               "sslProxy":PROXY,
                               "noProxy":None,
                               "proxyType":"MANUAL",
                               "class":"org.openqa.selenium.Proxy",
                               "autodetect":False
                               })
                profile.set_proxy(proxy)
#                 profile.set_preference("network.proxy.type", 1)
#                 profile.set_preference("network.proxy.http","127.0.0.1")
#                 profile.set_preference("network.proxy.http_port",3128)
#                 print "webdriver proxy ip:",proxy[:proxy.index(":")]
#                 profile.set_preference("network.proxy.https",proxy[:proxy.index(":")])
#                 print "webdriver proxy port:",proxy[proxy.index(":")+1:]
#                 profile.set_preference("network.proxy.https_port",int(proxy[proxy.index(":")+1:]))
                
#                 profile.set_preference("network.proxy.ssl","127.0.0.1")
#                 profile.set_preference("network.proxy.ssl_port",3128)  
    #             profile.set_preference("network.proxy.ftp",PROXY_HOST)
    #             profile.set_preference("network.proxy.ftp_port",int(PROXY_PORT))   
#                 profile.set_preference("network.proxy.socks","127.0.0.1")
#                 profile.set_preference("network.proxy.socks_port",3128)   
            profile.set_preference("general.useragent.override","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36")
            profile.update_preferences()
            print "webdriver.path:",config.get("production","webdriver.path")
            browser = webdriver.Firefox(executable_path=config.get("production","webdriver.path"), firefox_profile=profile)
        elif(webType == "phantomjs"):
            dcap = dict(DesiredCapabilities.PHANTOMJS)
#             dcap["phantomjs.page.settings.userAgent"] = (
#             "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/53 (KHTML, like Gecko) Chrome/15.0.87"
#             )
            dcap["phantomjs.page.settings.userAgent"] = (
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) " +
                "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36"
            )
            print "webdriver.path:",config.get("production","phantomjs.path"),"url:",url
#             '--ignore-ssl-errors=true', '--ssl-protocol=TLSv1',
# '--proxy=217.156.252.118:8080','--proxy-type=https',
# '--ignore-ssl-errors=true', '--ssl-protocol=TLSv1',
# '--proxy=127.0.0.1:9050',' --proxy-type=socks5',
            if proxy is None:
                print "proxy is None"
                browser = webdriver.PhantomJS(executable_path=config.get("production","phantomjs.path"), service_args=['--ignore-ssl-errors=true', '--ssl-protocol=TLSv1'], desired_capabilities=dcap)
            else:
                print "proxy is Not None",proxy
                browser = webdriver.PhantomJS(executable_path=config.get("production","phantomjs.path"), service_args=['--proxy='+proxy,'--proxy-type=https'], desired_capabilities=dcap)
        browser.set_window_size(1920, 1080)
        browser.get(url)
        if(webType == "webdriver"): 
            try:
                myElem = WebDriverWait(browser, 30).until(EC.presence_of_element_located((By.ID, 'captchaimglogin')))
                print "Page is ready!"
            except TimeoutException as e:
                print "Loading took too much time!",str(e)
        else:
            time.sleep(20)
        if(secondurl != None):
            browser.get(secondurl)
            time.sleep(10)
        return browser
    def get_captcha(self, snapshotPath, position):
        return Image.open(snapshotPath).crop(position).resize((int(180), int(40)), Image.ANTIALIAS)
    def login(self,c, account, password, browser, validationText):
        con = c.cursor()
        time.sleep(randint(5, 10))
        browser.find_element_by_name("username").send_keys(account)
        time.sleep(randint(1, 2))
        browser.find_element_by_name("password").send_keys(password)
        time.sleep(randint(1, 2))
        browser.find_element_by_name("captcha").send_keys(validationText)
        time.sleep(randint(10, 15))
        browser.find_element_by_name("login").click()
        time.sleep(10)
        try:
            result = browser.find_element_by_class_name("error_box").text
        except Exception as e:
            result = ""
            print str(e)
        print "result:", result
        if("Invalid Image Verification" in result):
            return False
        elif ("Your account is suspended" in result):
                udate_result = con.execute("UPDATE USERINFO SET STATUS = ?  WHERE USERNAME = ?",(result,account))
                print "update_result:",udate_result
                c.commit()
                return False
        elif("Invalid login details" in result):
            return False
        elif (result == ""):
            return True
        else:
            browser.execute_script("$('.ui-icon-closethick').click()")
            return True
        return True
    
    def logout(self, browser):
        browser.find_element_by_link_text("Logout").click()
        time.sleep(15 + randint(15, 20))
        browser.quit()
        exit()
    def validationCookie(self,cookieSession):
        response = None
#         if username is None or not os.path.exists("./cookies/cookies"+username+".json"):
#             self.logger.warning("json file not found")
#             return False
        if cookieSession is None:
            self.logger.warning("cookie not found")
            return False
#         cookiefile = open("./cookies/cookies"+username+".json","rb")
        try:
            s = requests.Session()
            for line in cookieSession.splitlines():
                cookie = json.loads(line)
                s.cookies.set(cookie['name'], cookie['value'])
#             session.close()
            response = requests.get("http://www.indexclix.com/index.php?view=ads",cookies=s.cookies)
        except Exception as e:
            print str(e)
            return False



#        webdriver的寫法
#             try:
#                 cookies = pickle.load(open("./cookies/cookies"+username+".pkl", "rb"))
#             except:
#                 self.logger.warning("validation is Exception")
#                 return False
#             s = requests.Session()
#             for cookie in cookies:
#                 s.cookies.set(cookie['name'], cookie['value'])
#                 print "cookies:",cookie['name'],cookie['value']
#             try:
#                 response = s.get("http://www.indexclix.com/index.php?view=ads")
#             except TooManyRedirects:
#                 return False
        bodyStr = response.text
        if '<li><a href="index.php?view=account" class="login">My Account</a></li>' in bodyStr:
            return True
        else:
            return False
        
    def saveCookie(self,c, browser,username):
#         if(driver=="webdriver"):
            # webdriver只要這樣寫就好
#             pickle.dump(browser.get_cookies() , open("./cookies/cookies"+username+".pkl", "wb"))
            json_string_all = ""
            for cookie in browser.get_cookies():
                json_string = json.dumps(cookie)
                json_string_all += json_string+"\n"
#             file = open("./cookies/cookies"+username+".json","wb")
            conn = c.cursor()
            conn.execute("update userinfo set SESSION = ? where username = ?",(json_string_all,username))
            c.commit()
#             file.write(json_string_all)
#             file.close()
    def loadCookie(self,c,browser,username):
        conn = c.cursor()
        session = conn.execute("select session from userinfo where username = ?",(username,))
        
        if not session is None:
            session = session.fetchone()[0]
#             session = session.decode("utf-8")
            if self.validationCookie(session):
    #             cookiefile = open("./cookies/cookies"+username+".json","rb")
                for line in session.splitlines():
                    dict = json.loads(line)
                    try:
                        browser.add_cookie(dict)
                    except WebDriverException as e:
                        print "add cookie error ,",line,"[ERROR]",str(e)
                        continue
    #             cookiefile.close()
                self.logger.warning("cookie is load finish")
                return True
            else:
                self.logger.warning("cookie is refresh")
                return False
        return False
#    如果你只要用webdriver不需要phantomjs可以這樣寫
#         if(driver=="webdriver"):
#             if self.validationCookie(username,driver):
#                 cookies = pickle.load(open("./cookies/cookies"+username+".pkl", "rb"))
#                 for cookie in cookies:
#                     browser.add_cookie(cookie)
#                 return True
#             else: 
#                 return False
    
    # String account: 帳號
    # String password: 密碼
    # String driver: 使用哪一種方式 webdirver or phantomjs 
    # String url: 訪問登入的頁面
    # String loginSnapshotName: 登入的快照用於擷取登入驗證碼名稱
    # String validationSnapshotName: 廣告的驗證碼快照名稱
    # String than 驗證模式（more or less or '')
    # String modelPath 要 restore的模型路徑
    # String mode (debug or '')
    
    # example:"webdriver","http://www.goldenclix.com/index.php?view=login","goldenClixIndex",(1129,584,1277,616),"View Advertisements","goldenSnapshot"
    
    def run(self,c, account, password, driver, url, than, modelPath, flowName,mode,proxy):
        successCount = 0
        failCount = 0
        loginSnapshotPath = r"./resource/"+flowName + "Index.jpg"
        validationSnapshotPath = r"./resource/"+flowName+"Snashot.jpg"
        tf.global_variables_initializer()
        foldername = time.strftime("%Y%m%d")
        finish = False
#         all_vars = tf.trainable_variables()
        if not os.path.exists("./fail_image"):
            os.mkdir("./fail_image")
        if not os.path.exists("./success_image"):
            os.mkdir("./success_image")
        if (not os.path.exists("./fail_image/" + foldername)):
            print "make now folder"
            os.mkdir("./fail_image/" + foldername)
        for step in range(0, 5, 1):
            tf.global_variables_initializer()
            with tf.Session() as sess:
                self.saver.restore(sess, tf.train.latest_checkpoint(modelPath))
                print "checkpoint:", tf.train.latest_checkpoint(modelPath)
                predict = tf.argmax(tf.reshape(self.output, [-1, MAX_CAPTCHA, CHAR_SET_LEN]), 2)
                # 初始化瀏覽器
                browser = self.initBrowser(driver, url,proxy,secondurl=None)
                time.sleep(10)
                browser.save_screenshot(loginSnapshotPath)
                # 取得驗證碼
                location = browser.find_element_by_id("captchaimglogin").location
                print "login captcha location:", location
                x = location['x']
                y = location['y']
                image = self.get_captcha(loginSnapshotPath, (int(x), int(y), int(x + 149), int(y + 33)))
                if (mode == "debug"):
                    image.show()
                validation_image = np.array(image) 
                captcha_image = convert2gray(validation_image)
                captcha_image = captcha_image.flatten() / 255
                text_list = sess.run(predict, feed_dict={X: [captcha_image], keep_prob: 1})
                text = text_list[0].tolist()  # 應該是機率最高的那一組
                vector = np.zeros(MAX_CAPTCHA * CHAR_SET_LEN)
                i = 0
                for n in text:
                        vector[i * CHAR_SET_LEN + n] = 1  # ex:ABCDE vector[1*26+65] vector[2*26+66] vector[3*26+67] ...
                        i += 1
                predict_text = vec2text(vector)
                print predict_text
                savePicture = True
                # 登入
                if self.loadCookie(c,browser,account):
                    print "loadCookie"
                    result = True
                    savePicture = False
                else:
                    result = self.login(c,account, password, browser, predict_text)
                if(result):
                    self.saveCookie(c,browser,account)
                    print "saveCookie"
                    successCount += 1
                    if(savePicture):
                        image.convert('RGB').save("./success_image/" + predict_text + ".png")
                    print "[SUCCESS]predict_text:", predict_text, " step:", step
                    con = c.cursor()
                    con.execute("UPDATE USERINFO SET ERRORCOUNT = ?,UPDATETIME=? WHERE USERNAME = ?",(0,datetime.datetime.now()+timedelta(days=1),account))
                    c.commit()
                    time.sleep(10)
                    # 自動點擊廣告
                    windowhandle = browser.current_window_handle
#                     userStep = open("./resource/step","wb")
#                     userStep.write("")
#                     userStep.close()
                    while(True):
                        if(flowName == "indexClixFlow"):
                            browser.switch_to_window(windowhandle)
                            flow = indexClixFlow.flow(c,account,browser, windowhandle,validationSnapshotPath, than, driver)
                            status = flow.flow_validation_result()
#                             self.updateMoney(c,browser,account)
                        if status:
#                             browser.quit()
                            sys.exit()
                            break
                        else:
                            self.logger.warning("kill phantomjs")
                            #OSError: [Errno 2] No such file or directory
                            try:
                                call(["killphantomjs"])
                                break
                            except Exception as e:
                                print str(e)
                                break
                else:
                    print "test account :",account
                    con = c.cursor()
                    rows = con.execute("SELECT ERRORCOUNT FROM USERINFO WHERE USERNAME=?;",(account,))
                    errorCount = rows.fetchone()[0]
                    con.close()
                    con = c.cursor()
                    con.execute("UPDATE USERINFO SET ERRORCOUNT = ?,UPDATETIME=? WHERE USERNAME = ?",(errorCount+1,datetime.datetime.now()+timedelta(days=1),account))
                    c.commit()
                    failCount += 1
                    image.convert('RGB').save("./fail_image/" + foldername + "/" + predict_text + ".png")
                    self.logger.warning( "[ERROR]predict_text:"+str(predict_text)+" step:"+str(step))
                    try:
                        browser.quit()
                    except Exception as e:
                        print str(e)
                        self.logger.warning("forcibly kill this browser") 
                    if errorCount >= 2: 
                        break
            #         print("正确: {}  预测: {}".format(correct_text, predict_text))
            #     predict_text = crack_captcha(image)
                print "successCount:", str(successCount), " failCount:", str(failCount)
                try:
                    self.logger.warning("ready logout ok")
                    login = browser.find_element_by_class_name("login").text
                    if "MY ACCOUNT" in login:
                        browser.find_element_by_class_name("login").click()
                        time.sleep(10)
                        f = open("./userInfo/" + account, "a")
                        f.write("money=" + browser.find_element_by_tag_name("strong")[1].text + "\n")
                        self.logger.warning("your money is writer userInfo dir, good jog!")
                        f.close()
                    self.logout(browser)
                    self.logger.warning("logout success!")
                    con = c.cursor()
                    con.execute("UPDATE USERINFO SET ERRORCOUNT = ? WHERE USERNAME = ?",(0,account))
                    c.commit()
                except Exception as e:
                    print str(e)
                    self.logger.warning("logout fail or not login")
                    try:
                        browser.quit()
                    except:
                        self.logger.warning("forcibly kill this browser") 
                        pass
        #     except:
                if(finish):
                        self.logger.warning("this process is finish it's task so good bye,next meet you :)")
                        break
        try:
            browser.quit()
        except Exception as e:
            print str(e)
            self.logger.warning("forcibly kill this browser") 
            pass
            
            
    def validation_captcha(self, browser, account, password, text):
            browser.find_element_by_name("username").send_keys(account)
            browser.find_element_by_name("password").send_keys(password)
            browser.find_element_by_name("captcha").send_keys(text)
            browser.find_element_by_name("login").click()
            time.sleep(randint(3, 6))
            try:
                result = browser.find_element_by_class_name("error_box").text
                if(result == "Invalid Image Verification"):
                    return False
                elif (result == "Your account is suspended."):
                    return True
            except Exception as e:
                print str(e)
                pass
            return False
    def start_login(self, account, password, driver, loginurl, loginSnapshotPath, modelPath, secondurl):
        browser = self.initBrowser(driver, loginurl)        
#         browser.get(url)
        result = False
        for step in range(0, 5, 1):
            print "golbal_variables_initialzer"
            tf.global_variables_initializer()
            with tf.Session() as sess:
                self.saver.restore(sess, tf.train.latest_checkpoint(modelPath))
                print "checkpoint:", tf.train.latest_checkpoint(modelPath)
                predict = tf.argmax(tf.reshape(self.output, [-1, MAX_CAPTCHA, CHAR_SET_LEN]), 2)
                browser.save_screenshot(loginSnapshotPath)
                # 取得驗證碼
                location = browser.find_element_by_id("captchaimglogin").location
                print "location:", location
                x = location['x']
                y = location['y']
                image = self.get_captcha(loginSnapshotPath, (x, y, x + 149, y + 33))
                validation_image = np.array(image) 
                captcha_image = convert2gray(validation_image)
                captcha_image = captcha_image.flatten() / 255
                text_list = sess.run(predict, feed_dict={X: [captcha_image], keep_prob: 1})
                text = text_list[0].tolist()  # 應該是機率最高的那一組
                vector = np.zeros(MAX_CAPTCHA * CHAR_SET_LEN)
                i = 0
                for n in text:
                        vector[i * CHAR_SET_LEN + n] = 1  # ex:ABCDE vector[1*26+65] vector[2*26+66] vector[3*26+67] ...
                        i += 1
                predict_text = vec2text(vector)
                print predict_text
                if self.loadCookie(browser):
                    print "loadCookie"
                    result = True
                else:
                    result = self.login(account, password, browser, predict_text)
                if(result):
                    self.saveCookie(browser)
                    print "saveCookie"
                browser.get(secondurl)
                time.sleep(5)
        return result
#                 clicklist = browser.find_element_by_class_name("clixgrid_clicked").find_element_by_tag_name("td")
#                 for click in clicklist:
#                     print "onmouseover;",click.get_attribute("onmouseover")
#                     try:
#                         print "title:",click.get_attribute("title")
#                     except:
#                         print "not find attribute title"
#                     try:
#                         print "onclick:",click.get_attribute("onclick")
#                     except:
#                         print "not find attribute onclick"
#                 
        
    # 很危險,請小心使用！！！！使用時請先接到手機wifi 避免ISP的IP被檔,帳號被鎖住
    # "webdriver","https://www.indexclix.com/index.php?view=login","./model","equalhsiao","dice442sue543","indexCliIndex.jpg",(1129,584,1277,616),500        
#     def web_guess_captcha(self,webType,url,loadmodel,account,password,loginSnapshotPath,loginposition,loopRange):
#         successCount = 0
#         failCount = 0
#         output = crack_captcha_cnn()
#         saver = tf.train.Saver()
#         foldername=time.strftime("%Y%m%d")
#         if (not os.path.exists('/home/master/'+foldername)):
#             os.mkdir("/home/master/"+foldername)
#         with tf.Session() as sess:
#             saver.restore(sess, tf.train.latest_checkpoint(loadmodel))
#             print "checkpoint:",tf.train.latest_checkpoint(loadmodel)
#             predict = tf.argmax(tf.reshape(output, [-1, MAX_CAPTCHA, CHAR_SET_LEN]), 2)
#             browser = self.initBrowser(webType,url)
#             browser.save_screenshot(loginSnapshotPath)
#             for step in range(0,loopRange,1):
#                 img = self.get_captcha(browser,loginSnapshotPath,loginposition)
#                 validation_image = np.array(img) 
#                 captcha_image = convert2gray(validation_image)
#                 captcha_image = captcha_image.flatten() / 255
#                 text_list = sess.run(predict, feed_dict={X: [captcha_image], keep_prob: 1})
#                 text = text_list[0].tolist()  #應該是機率最高的那一組
#                 vector = np.zeros(MAX_CAPTCHA*CHAR_SET_LEN)
#                 i = 0
#                 for n in text:
#                         vector[i*CHAR_SET_LEN + n] = 1  #ex:ABCDE vector[1*26+65] vector[2*26+66] vector[3*26+67] ...
#                         i += 1
#                 predict_text =  vec2text(vector)
#                 result = self.validation_captcha(browser,account,password,predict_text)
#                 if(result):
#                     successCount += 1
#                     img.convert('RGB').save("/home/master/success_image/"+predict_text+".png")
#                     print "[SUCCESS]predict_text:",predict_text," step:",step
#                 else:
#                     failCount += 1
#                     img.convert('RGB').save("/home/master/"+foldername+"/"+predict_text+".png")
#                     print "[ERROR]predict_text:",predict_text," step:",step
        #         print("正确: {}  预测: {}".format(correct_text, predict_text))
        #     predict_text = crack_captcha(image)
#             print "successCount:",str(successCount)," failCount:",str(failCount)
    def guess_captcha(self, image, modelPath, output, saver):
        tf.global_variables_initializer()
        foldername = time.strftime("%Y%m%d")
        all_vars = tf.trainable_variables()
        if (not os.path.exists('./fail_image/' + foldername)):
            os.mkdir("./fail_image/" + foldername)
        with tf.Session() as sess:
#             tf.reset_default_graph()
            saver.restore(sess, tf.train.latest_checkpoint(modelPath))
            print "checkpoint:", tf.train.latest_checkpoint(modelPath)
            predict = tf.argmax(tf.reshape(output, [-1, MAX_CAPTCHA, CHAR_SET_LEN]), 2)
#             for step in range(0,100,1):
            captcha_image = convert2gray(np.array(image))
            captcha_image = captcha_image.flatten() / 255
            text_list = sess.run(predict, feed_dict={X: [captcha_image], keep_prob: 1})
            predict_text = text_list[0].tolist()  # 應該是機率最高的那一組
            vector = np.zeros(MAX_CAPTCHA * CHAR_SET_LEN)
            i = 0
            for n in predict_text:
                    vector[i * CHAR_SET_LEN + n] = 1  # ex:ABCDE vector[1*26+65] vector[2*26+66] vector[3*26+67] ...
                    i += 1
            predict_text = vec2text(vector)
            print "預測:", predict_text
            return predict_text
            
    def autoRegister(self,c, webType, url,secondurl, email, modelPath,proxy,ip):
        con = c.cursor()
        loop = True
#         try:
        browser = self.initBrowser(webType, url,proxy,secondurl)
#         except:
#             p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
#             out, err = p.communicate()
#             for line in out.splitlines():
#                 if 'iChat' in line:
#                     pid = int(line.split(None, 1)[0])
#                     os.kill(pid, signal.SIGKILL)
#             if subprocess.call( [ "killall", "-9", "firefox" ] ) > 0:
#                 self._logger.debug( 'Firefox cleanup - FAILURE!' )
#             else:
#                 self._logger.debug( 'Firefox cleanup - SUCCESS!' )
#             import subprocess, signal
#             p = subprocess.Popen(['ps', 'aux'], stdout=subprocess.PIPE)
#             out, err = p.communicate()
#             for line in out.splitlines():
#                 if 'filefox' in line:
#                     pid = int(line.split(None, 10)[1])
#                     print line
#                     os.kill(pid, signal.SIGKILL)
#             return "initBroserError"
            
        browser.set_window_size(1920, 1080)
#         IP = IPTools().getIP("phantomjs", "http://www.whatismyip.com.tw/", "span")
#         print IP
        result = None
        while(loop):
            waitTime = 30
            try:
                browser.refresh()
            except TimeoutException as e:
                print "Message: Timeout loading page after 300000ms",str(e)
                pass
            time.sleep(waitTime)
            try:
                browser.find_element_by_name("fullname").clear()
                browser.find_element_by_name("username").clear()
                browser.find_element_by_name("email").clear()
                browser.find_element_by_name("email2").clear()
                browser.find_element_by_name("password").clear()
                browser.find_element_by_name("password2").clear()
                browser.find_element_by_name("captcha").clear()
            except:
                browser.save_screenshot("debug.jpg")
                print "get element error watit time add 10 seconds retry"
                waitTime += 10
                continue
            fullname = "".join([chr(randint(0, 25) + 65) for i in  range(0, randint(6, 12))])
            username = "".join([chr(randint(0, 25) + 65) for i in  range(0, randint(5, 9))])
            password = "".join([chr(randint(0, 25) + 97) for i in  range(0, randint(6, 12))])
            browser.save_screenshot("./resource/register.jpg")
            time.sleep(5)
            size = browser.find_element_by_id("captchaimg").location
            print "size:",size
            image = self.get_captcha("./resource/register.jpg", (int(size['x']), int(size['y']), int(size['x'])+148, int(size['y'])+32))
            captcha = self.guess_captcha(image, modelPath, self.output, self.saver)
            browser.find_element_by_name("fullname").send_keys(fullname)
            browser.find_element_by_name("username").send_keys(username)
            browser.find_element_by_name("email").send_keys(email)
            browser.find_element_by_name("email2").send_keys(email)
            browser.find_element_by_name("password").send_keys(password)
            browser.find_element_by_name("password2").send_keys(password)
            browser.find_element_by_name("captcha").send_keys(captcha)
            browser.find_element_by_name("terms").click()
            time.sleep(randint(1, 3))
            browser.find_element_by_name("login").click()
            if(not os.path.exists("./userInfo")):
                os.mkdir("./userInfo")
            time.sleep(3)
            try:
                result = browser.find_element_by_class_name("error_box").text
                print "result:", result
                if(result == "Invalid Image Verification"):
                    print "[CAPTCHA-ERROR]predict_text:", captcha ," [LOGIN-ERROR] your accoun is suspended"
                    image.save("./fail_image/" + captcha + ".png")
                    image.close()
                    loop = True
                    
                elif (result.strip() == "" or "Your account is suspended." in result):
                    print "[CAPTCHA-SUCCESS]predict_text:", captcha , " [LOGIN-ERROR] your accoun is suspended"
                    image.save("./success_image/" + captcha + ".png")
                    image.close()
                    con = c.cursor()
                    con.execute("UPDATE USERINFO SET STATUS = ? ,UPDATETIME = ? WHERE USERNAME = ?",(result,datetime.datetime.now()+timedelta(days=1),username))
                    c.commit()
                    loop = False
                elif("Sorry IP is already used by other member" in result):
                    print "[CAPTCHA-SUCCESS]predict_text:", captcha , "[LOGIN-ERROR] Sorry IP is already used by other member"
                    con = c.cursor()
                    con.execute("UPDATE PROXYSERVER SET STATUS = ? ,UPDATETIME = ? WHERE ID = ?",(result,datetime.datetime.now()+timedelta(days=1),proxy))
                    c.commit()
                    loop = False
                elif (result.strip() == "Invalid login details."):
                    print "[CAPTCHA-SUCCESS]predict_text:", captcha, " [LOGIN-ERROR] Invalid login details."
                    image.save("./success_image/" + captcha + ".png")
                    image.close()
                    loop = False
                else:
                    print "[ERROR]predict_text:", captcha, " [LOGIN-ERROR] None."
                    image.save("./fail_image/" + captcha + ".png")
                    image.close()
                    loop = True
            except:
                time.sleep(5)
                result = browser.find_elements_by_tag_name("h3")[0].text
                if("Welcome to IndexClix" in result):
                    image.save("./success_image/" + captcha + ".png")
#                     image.close()
                    print "[SUCCESS]predict_text:", captcha ,"[LOGIN-SUCCESS] Welcome to IndexClix"
                    if proxy is None:
                        con.execute("INSERT INTO USERINFO (FULLNAME,USERNAME,EMAIL,PASSWORD,IP,STATUS,CLASS,CREATETIME,ERRORCOUNT,ISPROXY) VALUES (?,?,?,?,?,?,?,?,?,?)",(fullname,username,email,password,ip,result,"indexClix",datetime.datetime.now() + timedelta(days=1),0,False))
                    else:
                        con.execute("INSERT INTO USERINFO (FULLNAME,USERNAME,EMAIL,PASSWORD,IP,STATUS,CLASS,CREATETIME,ERRORCOUNT,ISPROXY) VALUES (?,?,?,?,?,?,?,?,?,?)",(fullname,username,email,password,proxy,result,"indexClix",datetime.datetime.now() + timedelta(days=1),0,True))
                    c.commit()
                    loop = False
                else:
                    print "login error please retry"
                    self.logger.warning("login error please retry")
                    pass
#             image.save("/home/master/fail_image/"+captcha+".png")
#             print "[ERROR]predict_text:",captcha
        browser.quit()
        return result
#         browser.find_element_by_name("login").click()
#         return browser,fullname,username,password
        
    

        
        
        
