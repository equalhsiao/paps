# -*- coding: utf-8 -*- 
'''
Created on 2017年7月24日

@author: siao
'''
from PIL import Image
from _socket import error
import datetime
from numpy.random.mtrand import randint
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time


#驗證廣告圖片
def cutImageValidation(image):
    #(36,36)
    imagelist = []
    image1 = image.crop((493, 8, 517, 32)).convert("1")
    print image1.size
    imagelist.append(image1)
    image2 = image.crop((543, 8, 567, 32)).convert("1")
    print image2.size
    imagelist.append(image2)
    image3 = image.crop((593, 8, 617, 32)).convert("1")
    #643,8~667,32
    #693,8~717,32
    #743,8~767,32
    print image3.size
    imagelist.append(image3)
    image4 = image.crop((643, 8, 667, 32)).convert("1")
    print image4.size
    imagelist.append(image4)
    image5 = image.crop((693, 8, 717, 32)).convert("1")
    print image5.size
    imagelist.append(image5)
    image6 = image.crop((743, 8 ,767, 32)).convert("1")
    print image6.size
    imagelist.append(image6)
    less_block_position = 0;
    less_black = 500
    for step,im in enumerate(imagelist):
        print step
        width,height = im.size
        block_point = 0
        for w in range(0,width):
            for h in range(0,height):
                if(im.getpixel((w,h)) == 255):
                    block_point +=1
        print "block_point:",block_point 
        if(less_black > block_point):
            less_black = block_point
            less_block_position = step
    print "less_black:",less_black,"less_block_position:",less_block_position
    return less_block_position
#初始化瀏覽器
def initBrowser(type,url):
    if(type=="webdriver"):
        profile = webdriver.FirefoxProfile()
        browser = webdriver.Firefox(executable_path=r'/home/master/geckodriver',firefox_profile=profile)
    elif(type=="phantomjs"):
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        dcap["phantomjs.page.settings.userAgent"] = (
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/53 (KHTML, like Gecko) Chrome/15.0.87"
        )
        browser = webdriver.PhantomJS(executable_path="/home/master/phantomjs-2.1.1-linux-x86_64/bin/phantomjs",service_args=['--ignore-ssl-errors=true', '--ssl-protocol=TLSv1'],desired_capabilities=dcap)
    browser.set_window_size(1920, 1080)
    print url
    browser.get(url)
    time.sleep(15)
    browser.find_element_by_id("captcha_login").click()
    browser.close()
    #切到主要頁面(最後一個)
    for window in browser.window_handles:
        browser.switch_to_window(window)
    time.sleep(7)
#     browser.execute_script("window.scrollTo(0, document.body.scrollHeight/3);")
    browser.save_screenshot("goldenIndex.jpg")
    return browser
def get_captcha():
    image = Image.open("goldenIndex.jpg")
    image1 = image.crop((547, 713, 726, 752))
    img = image1.resize((180,40), Image.ANTIALIAS)
    return img
def login(browser,text):
    time.sleep(randint(5,10))
    browser.find_element_by_name("username").send_keys("equalhsiao")
    time.sleep(randint(1,2))
    browser.find_element_by_name("password").send_keys("dice442sue543")
    time.sleep(randint(1,2))
    browser.find_element_by_name("captcha").send_keys(text)
    time.sleep(randint(1,2))
    browser.find_element_by_name("login").click()
    time.sleep(randint(5,10))
    try:
        result = browser.find_element_by_class_name("error_box").text
        print "result:",result
        if(result=="Invalid Image Verification"):
            return False
        elif (result=="Your account is suspended."):
            time.sleep(randint(15,20))
            return True
        elif (result==""):
            time.sleep(randint(1,5))
            return True
        else:
            browser.execute_script("$('.ui-icon-closethick').click()")
            return True
    except:
        pass
    return True

#自動點廣告     
def autoClickAD(browser):
#     browser.get("http://www.goldenclix.com/index.php?view=ads")
    print "title:",browser.title
#     try:
    while(True):
        browser.find_element_by_id("dropdown-toggle").click()
        time.sleep(randint(2,4))
        browser.find_element_by_link_text("View Advertisements").click()
        time.sleep(15)
        validation_adblock = len(browser.find_elements_by_class_name("ad-block disabled"))
        print "validation_adblock:",validation_adblock
        success_count = 1
        for step in range(0,len(browser.find_elements_by_class_name("ad-block"))-1):
            print "Step",step
            element = browser.find_elements_by_class_name("ad-block")[step]
            className =  element.get_attribute("class")
            if(className in "disabled"):
                continue
            pointer_element = browser.find_elements_by_xpath("//span[@class='pointer']")[step]
            pointer_element.click()
            for handle in browser.window_handles:
                browser.switch_to.window(handle)
                print "title:",browser.title
                for i in range(0,3):
                    time.sleep(10+randint(5,10))
                    errobox = ""
                    try:
                        errobox = browser.find_elements_by_class_name("errorbox").text
                    except:
                        pass
                    if errobox=="You already visited this advertisement in the last 24 hours":
                        browser.close()
                        continue
            browser.save_screenshot("goldenClixScreenshot.jpg")
            time.sleep(1)
            image = Image.open("goldenClixScreenshot.jpg")
            position = cutImageValidation(image)
            try:
                browser.execute_script("endprogress("+str(position+1)+");")
            except:
                browser.close()
                continue
            time.sleep(randint(3,5))
            browser.close()
            success_count +=1
            if success_count % 1==0:
                break
                browser.switch_to_window(browser.window_handles[0])
                time.sleep(20+randint(5,10))
        if validation_adblock >= len(browser.find_elements_by_class_name("ad-block")):
            break
        
    return True
#     except:
#             raise
#登出
def logout(browser):
# time.sleep(randint(10,15))
    browser.find_element_by_link_text("Logout").click()
    time.sleep(15+randint(15,20))
    browser.close()
    exit()


if __name__ == "__main__": 
    #前置作業
    browser = initBrowser("webdriver","http://www.goldenclix.com/index.php?view=login")
    browser.find_element_by_id("captcha_login").click()
    browser.close()
    #切到主要頁面(最後一個)
    for window in browser.window_handles:
        browser.switch_to_window(window)
    loginFailCount = 0
    while True:
        result = login(browser,"")
        print result
        if result :
            break
        else:
            loginFailCount += 1
        if loginFailCount >= 2:
            time.sleep(randint(5,10))
            logout(browser)
            time.sleep(randint(15,20))
            
    time.sleep(randint(15,20))
    autoClickAD(browser)
    logout()
    