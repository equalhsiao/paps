# coding=utf-8
'''
Created on 2017年9月13日

@author: master
'''
import ConfigParser
from datetime import timedelta
import datetime
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import sqlite3
import time


if __name__ == '__main__':
    with open('./../../../DeepLearning.properties') as fp:
            config = ConfigParser.ConfigParser()
            config.readfp(fp)
            fp.close()
    dcap = dict(DesiredCapabilities.PHANTOMJS)
#             dcap["phantomjs.page.settings.userAgent"] = (
#             "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/53 (KHTML, like Gecko) Chrome/15.0.87"
#             )
    dcap["phantomjs.page.settings.userAgent"] = (
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) " +
        "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36"
    )
#     print "webdriver.path:",config.get("production","phantomjs.path"),"url:",url
#             '--ignore-ssl-errors=true', '--ssl-protocol=TLSv1',
# '--proxy=217.156.252.118:8080','--proxy-type=https',
# '--ignore-ssl-errors=true', '--ssl-protocol=TLSv1',
    browser = webdriver.PhantomJS(executable_path=config.get("production","phantomjs.path"), service_args=['--ignore-ssl-errors=true', '--ssl-protocol=TLSv1'], desired_capabilities=dcap)
    browser.set_window_size(1920, 1080)
    db = sqlite3.connect('./../indexClix/test.db')
    connection = db.cursor()
#         http://www.freeproxylists.net/zh/?s=u&page=1
    browser.get("http://www.freeproxylists.net/zh/?s=u")
    pages = browser.find_elements_by_class_name("page")[0].find_elements_by_tag_name("a")
    pageSize = len(pages)-2
    totalPage = int(pages[pageSize].get_attribute("innerText"))+1
    print "total:",totalPage
    for i in range(1,totalPage,1):
        print "totalPage:",totalPage,"page:",i
        browser.get("http://www.freeproxylists.net/zh/?s=u&page="+str(i))
        time.sleep(30)
        Odd = browser.find_elements_by_class_name("Even")
        Even = browser.find_elements_by_class_name("Odd")
        print "Odd len:",len(Odd)," Even len:",len(Even)
        proxy_list = Odd + Even
        print "All len:",len(proxy_list)
        for proxyServer in proxy_list:
            try:
                IP = proxyServer.find_elements_by_tag_name("a")[0].get_attribute("innerHTML")
                port = proxyServer.find_elements_by_tag_name("td")[1].get_attribute("innerHTML")
                proxyType = proxyServer.find_elements_by_tag_name("td")[2].get_attribute("innerHTML")
                Anonymity = proxyServer.find_elements_by_tag_name("td")[3].get_attribute("innerHTML")
                country = proxyServer.find_elements_by_tag_name("td")[4].get_attribute("innerText")
                area = proxyServer.find_elements_by_tag_name("td")[5].get_attribute("innerHTML")
                city = proxyServer.find_elements_by_tag_name("td")[6].get_attribute("innerHTML")
                Availability = proxyServer.find_elements_by_tag_name("td")[7].get_attribute("innerHTML").strip().replace("%","")
                ResponseSpeed = proxyServer.find_elements_by_tag_name("td")[8].find_elements_by_class_name("graph")[0].get_attribute("innerHTML")
                ResponseSpeed = ResponseSpeed[ResponseSpeed.index("width"):ResponseSpeed.index(";")].replace("%","").replace("width:","")
                transformSpeed = proxyServer.find_elements_by_tag_name("td")[9].find_elements_by_class_name("graph")[0].get_attribute("innerHTML")
                transformSpeed = transformSpeed[transformSpeed.index("width"):transformSpeed.index(";")].replace("%","").replace("width:","")
                print "log:",proxyType+":"+IP+":"+port,IP,port,proxyType,Anonymity,country,area,city,Availability,ResponseSpeed,transformSpeed,"http://www.freeproxylists.net/zh/?s=u"
                try:
                    connection.execute("INSERT INTO PROXYSERVER (ID,IP,PORT,PROXYTYPE,ANONYMITY,COUNTRY,AREA,CITY,AVAILABILITY,RESPONSESPEED,TRANSFORMSPEED,SOURCE,CREATETIME) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",(proxyType+":"+IP+":"+port,IP,port,proxyType,Anonymity,country,area,city,Availability,ResponseSpeed,transformSpeed,"""http://www.freeproxylists.net/zh/?s=u&page="""+str(i),datetime.datetime.now() + timedelta(days=1)))
                    db.commit()
                except sqlite3.IntegrityError:
                    print "pass UNIQUE constraint failed: PROXYSERVER.ID"
            except IndexError:
                pass

        
    
