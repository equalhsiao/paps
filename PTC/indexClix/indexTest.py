# coding=utf-8
'''
Created on 2017年7月30日

@author: master
'''
import ConfigParser
from datetime import timedelta
import datetime
import getpass
import os
import sqlite3
import sys
import time
sys.path.append("/home/"+getpass.getuser()+"/workspace/PAPS")
from PTC.common.tools import tools










# from PTC.common.emailTools import FakeEmail
def findUserInfo(c,ip):
#         con.execute('''CREATE TABLE USERINFO
#              (FULLNAME text, USERNAME text, EMAIL text, PASSWORD text, IP text,STATUS text,CLASS text)''')
        con = c.cursor()
        IP = ip
        user=None
        pwd=None
        rows = con.execute("SELECT USERNAME, PASSWORD, IP,ISPROXY from USERINFO where IP='%s'" %IP)
        row = rows.fetchone()
        print row
        if not row is None:
            print "USERNAME:",row[0],"PASSWORD:",row[1],"IP:",row[2],"ISPROXY",row[3]
            return row[0],row[1],row[2],row[3]
        else:
            return None,None,None,None
#         for f in fileList:
#             config = ConfigParser.ConfigParser()
#             try:
#                 config.read("./userInfo/" + str(f))
#             except:
#                 print "config read error try by pass"
#             if(IP in config.get("default", "IP") ):
#                 user = config.get("default", "username")
#                 pwd = config.get("default", "password")
#                 print "fileList:", len(fileList),"config is read userInfo",
#                 print "username:" + user,"password:" + pwd,"IP:",IP
#                 return user,pwd
#         print " will return none"
#         print "fileList:", len(fileList),"config is read userInfo",
#         print "username:None password:None","IP:",IP
#         return None,None

def indexTestInit():
    if not os.path.exists("./resource"):
        os.mkdir("./resource")
    if not os.path.exists("./cookies"):
        os.mkdir("./cookies")
    if not os.path.exists("./userInfo"):   
        os.mkdir("./userInfo")
    if not os.path.exists("./split_text"):
        os.mkdir("./split_text")
        if not os.path.exists("./split_text/success_image"):
            os.mkdir("./split_text/success_image")
        if not os.path.exists("./split_text/fail_image"):
            os.mkdir("./split_text/fail_image")
if __name__ == "__main__":
#     image = Image.open("indexCliIndex.jpg").convert("RGB")
#     testimage = image.crop((1129,584,1277,616))
#     print "testimage:",testimage.size
#     image.close()
#     tool = tools()
#     image = Image.open("test.jpg")
    # (498,11,520,26), (548,11,570,26), (598,11,620,26), (648,11,670,26), (698,11,720,26), (748,11,770,26)
#     image.crop((498,11,520,26)).show()
#     less,more = tool.cutImageValidation(image,"", (498,11,520,26), (548,11,570,26), (598,11,620,26), (648,11,670,26), (698,11,720,26), (748,11,770,26))
#     print "less:",less,"more:",more
#     tool.validationCookie()
#     try:
        print "[開始]現在時間：",datetime.datetime.now()
        tool = tools()
        indexTestInit()
        real_IP = tool.getIP()
        print "real ip:",real_IP
#         proxy_IP = tool.getWebIp()
#         print "proxy ip:",proxy_IP
        conn = sqlite3.connect('test.db')
        fileList = os.listdir("./userInfo")
        username,password,IP,isProxy = findUserInfo(conn,real_IP)
        print username,password
        if (username is None and password is None) or (username is "" and password is ""):
#             https://10minutemail.com/10MinuteMail/index.html
#             https://10minutemail.net/?lang=zh-tw fe_text
#             proxy = tool.getProxyIP(conn)
            proxy = None
            tool.testWebIP(proxy)
            print "no mapping IP and username use register, use proxy:",proxy
            try:
                emailBrowser,email,createTime = tool.createEmail("phantomjs","https://10minutemail.com/10MinuteMail/index.html" ,"mailAddress")
            except Exception as e:
                print str(e)
                emailBrowser,email,createTime = tool.createEmail("phantomjs","https://10minutemail.org/","fe_text")
            print "email:",email," createTime:",createTime,"valid:",tool.validationEmail(int(datetime.datetime.now().strftime("%s")) * 1000)
            emailBrowser.close()
            result = tool.autoRegister(conn,"webdriver","https://www.indexclix.com/?ref=VHVFUZQLY","https://www.indexclix.com/index.php?view=register",email,"./indexCliModel",proxy,real_IP)
            print "indexTest result:",result
            if "Sorry IP is already used by other member" in result:
                con = conn.cursor()
                con.execute("select USERNAME,PASSWORD,IP,ISPROXY from userinfo where IP like ?",('%{}%'.format(real_IP[:(real_IP.index(".",real_IP.index(".")+1,len(real_IP)))]),))
                user_data = con.fetchone()
                username = user_data[0]
                password = user_data[1]
                IP = user_data[2]
                is_proxy = user_data[3]
            else:
                username,password,IP,is_proxy = findUserInfo(conn,real_IP)
#             con = conn.cursor()
#             if result == "initBroserError":
#                 con.execute("UPDATE proxyserver SET STATUS = '0',UPDATETIME = ?  WHERE ID = ? ",(datetime.datetime.now()+timedelta(days=1),'%{}%'.format(proxy)))
#                 conn.commit()
#             else:
#                 con.execute("UPDATE proxyserver SET STATUS = '1',UPDATETIME = ?  WHERE ID = ? ",(datetime.datetime.now()+timedelta(days=1),'%{}%'.format(proxy)))
#                 conn.commit()
#                 proxy = tool.getProxyIP(conn)
#             if "Sorry IP is already used by other member" in result:
#             else:
#                 fileList = os.listdir("./userInfo")
                
            if is_proxy:
                tool.run(conn,username,password,"webdriver","https://www.indexclix.com/index.php?view=login","more",'./../indexCliModel',"indexClixFlow","selenium",proxy)
            else:
                tool.run(conn,username,password,"webdriver","https://www.indexclix.com/index.php?view=login","more",'./../indexCliModel',"indexClixFlow","selenium",None)
            
        else:
            if (isProxy == 1):
                print "test is proxy",isProxy
                tool.run(conn,username,password,"webdriver","https://www.indexclix.com/index.php?view=login","more",'./../indexCliModel',"indexClixFlow","selenium",IP)
            else:
                print "test is no proxy",isProxy
                print "username:" + username,"password:" + password,"IP:",real_IP
                tool.run(conn,username,password,"webdriver","https://www.indexclix.com/index.php?view=login","more",'./../indexCliModel',"indexClixFlow","selenium",None)
#     except:
#         print "[結束]現在時間：",datetime.datetime.now()
#         sys.exit()
#         tool.run("ZUKXJEK","gyukeafyu","phantomjs","https://www.indexclix.com/index.php?view=login","more",'./indexCliModel',"indexClixFlow","phantomjs")
#     <td onmouseover="clixmove(1,1)" class="clixgrid_clicked" title="You have already clicked this position today."></td>
