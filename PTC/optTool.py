#coding=utf-8
'''
Created on 2017年7月17日

@author: master
'''
from PIL import Image
import os
from random import randint

import numpy as np


#去除多餘pixel
def checkPoint(x,y,a1,ignorex,ignorey,i):
    checkcount = 0
    if ignorex != "left" and ignorey != "down":
        checkPixel = a1.getpixel((x+i,y+i))
        if(checkPixel >= (220,220,220) and checkPixel <= (255,255,255)):
            checkcount += 1
    if ignorex != "right" and ignorey != "up":
        checkPixel = a1.getpixel((x-i,y-i))
        if(checkPixel >= (220,220,220) and checkPixel <= (255,255,255)  ):
            checkcount += 1
    if ignorex != "left" and ignorey != "up":
        checkPixel = a1.getpixel((x+i,y-i))
        if(checkPixel >= (220,220,220) and checkPixel <= (255,255,255) ):
            checkcount += 1    
    if ignorex != "left" and ignorey != "down":
        checkPixel = a1.getpixel((x-i,y+i))
        if(checkPixel >= (220,220,220) and checkPixel <= (255,255,255) ):
            checkcount += 1 
    return checkcount
#將背景色去除
def cleanbackgroup(width,height,a1):
    for x in range(0,width):
        for y in range(0,height):
            pixel = a1.getpixel((x,y))
            new_pixel  = None
            new_pixel = (220,220,220)
            R = pixel[0]
            G = pixel[1]
            B = pixel[2]
#             or( (R > 180 and R<255)  and (G > 180 and G < 255) and (B > 180 and B < 255) )
            if((R <= 1 and G <= 1 and B <= 1) or (R>=255 and G >= 255 and B >=255)  ):
                    R = new_pixel[0]
                    G = new_pixel[1]
                    B = new_pixel[2]
            a1.putpixel((x,y),(R,G,B))
#             print "(x:"+str(x)+")(y:"+str(y)+")",a1.getpixel((x,y)),",", 
#         print ""
    return a1
#去除第2多的顏色
def cleanExtra(width,height,a1,removeColor):
    print "removeColor:",removeColor
    for x in range(0,width):
        for y in range(0,height):
            pixel = a1.getpixel((x,y))
            if( ((pixel[0]+20,pixel[1]+20,pixel[2]+20) >= removeColor and (pixel[0]-20,pixel[1]-20,pixel[2]-20) <= removeColor) or (pixel[0],pixel[1],pixel[2]) == removeColor ):
                    a1.putpixel((x,y),(255,255,255))
            for j in range(0,2,1):
                checkcount = 0
                for i in range(0,3,1):
                    if((x+i < width-1 and y+i < height-1)  and (x-i > 0 and y-i > 0)):
                        checkcount += checkPoint(x,y,a1,"","",i)
                    else:    
                        if( x <= 0 and y <=0):
                            checkcount += checkPoint(x,y,a1,"left","up",i)
                        if (x >= width and y >= height):
                            checkcount += checkPoint(x,y,a1,"right","down",i)
                        if (x <= 0 and y >= height):
                            checkcount += checkPoint(x,y,a1,"left","down",i)
                        if (x >= width and y <= 0):
                            checkcount += checkPoint(x,y,a1,"right","up",i)
                if checkcount >= 3 :
                    a1.putpixel((x,y),(255,255,255))
    return a1
def findMainColor(width,height,a1):
    mainCount = 0
    mainColor = None
    secondColor = 0
    allColor = a1.getcolors(height*width) 
    print allColor
    print max(a1.getcolors(height*width))
    for im in allColor:
        if im[1] != (255,255,255) and im[0] > mainCount:
            mainCount = im[0]
            secondColor = mainColor
            mainColor = im[1]
    print "secondColor:",secondColor
    print "mainColor:",mainColor
    return mainColor,secondColor,a1
def mainThread(a1):
        width,height = a1.size
        a1 = cleanbackgroup(width,height,a1)
        mainColor,secondColor,a1 = findMainColor(width, height,a1)
#         if((mainColor[0]+30,mainColor[1]+30,mainColor[2]+30) >= (secondColor) and (mainColor[0]-30,mainColor[1]-30,mainColor[2]-30) <= (secondColor)  ):
#             a1 = cleanExtra(width,height,a1,secondColor)
        return a1
#         else:
#             return None
if __name__ == '__main__':
    DATA_DIR = "/home/master/images"
    files = os.listdir(DATA_DIR)    
    count = 0
    for filename in files:
        image = Image.open(DATA_DIR+"/"+filename)
        a1 = mainThread(image.crop((0,0,36,40)))
        a2 = mainThread(image.crop((36,0,72,40)))
        a3 = mainThread(image.crop((72,0,108,40)))
        a4 = mainThread(image.crop((108,0,144,40)))
        a5 = mainThread(image.crop((144,0,180,40)))
#         a1.show()
        if(a1 != None):
            a1.save("/home/master/imageTest/a1"+str(count)+".png")
#         a2.show()
        if(a2 != None):
            a2.save("/home/master/imageTest/a2"+str(count)+".png")
#         a3.show()
        if(a3 != None):
            a3.save("/home/master/imageTest/a3"+str(count)+".png")
#         a4.show()
        if(a4 != None):
            a4.save("/home/master/imageTest/a4"+str(count)+".png")
#         a5.show()
        if(a5 != None):
            a5.save("/home/master/imageTest/a5"+str(count)+".png")
        image.close()
        count += 1
        
 
    
    