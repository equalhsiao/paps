# coding=utf-8
'''
Created on 2017年8月1日

@author: master
'''
import json
import sys
import time

from PTC.common.tools import tools
from PTC.train_captcha import  convert2gray, X, keep_prob, \
    MAX_CAPTCHA, CHAR_SET_LEN, vec2text
import numpy as np
import tensorflow as tf


sys.path.append('../..')



if __name__ == "__main__":
#     pass
#     tool = tools()
#     filelist = os.listdir("/home/master/fail_image")
#     output = crack_captcha_cnn()
#     saver = tf.train.Saver()
#     for f in filelist:
#         image = Image.open("/home/master/fail_image/"+f)
#         tool.guess_captcha(image, "./../model",output,saver)
#     print IPTools().getIP("phantomjs","http://www.whatismyip.com.tw/","span")
#     username = "test"
#     password = "test"
#     fullname = "test"
#     email = "test"
#     check = "fuck"
#     IP = "127.0.0.1"
#     f = open("./userInfo/"+username,"a")
#     f.write("[global]\nfullname="+fullname+"\n"+"username="+username+"\n"+"email="+email+"\n"+"password="+password+"\n"+"IP="+IP+"\n"+"status="+check+"\n")
#     f.close()
#     config =  ConfigParser.ConfigParser()    
#     if os.path.exists("./userInfo"):
#         fileList = os.listdir("./userInfo")
#         for f in fileList:
#             config.read('./userInfo/'+f)
#             if(config.get('global','IP')==IP):
#                 username = config.get('global','username')
#                 print "username:"+username
#                 password = config.get('global','password')
#                 print "password:"+password
#     tool = tools()
#     testimage = Image.open("test.jpg")
#     browser = tool.initBrowser("webdriver", "https://www.indexclix.com/index.php?view=login")
#     location = browser.find_element_by_id("captchaimglogin").location
#     print "location:",location
#     x = location['x']
#     y = location['y']
#     browser.save_screenshot("indexCliIndex.jpg")
#     image = tool.get_captcha("indexCliIndex.jpg",(x,y,x+149,y+33)).show()
    
#     size = (498,11,520,26)
#     size = {'y': 58.0, 'x': 472.0}
#     area = {'y': 8.0, 'x': 472.0}
#     tool.cutImageValidation(testimage,"",size,area,"debug")

    driver = "phantomjs"
    url = "https://www.indexclix.com/index.php?view=login"
    loginSnapshotPath = "cookiesTest.jpg"
    mode = "debug"
    tool = tools()
    browser = tool.initBrowser(driver, url, secondurl=None)
    browser.find_element_by_name("username").send_keys("GYPVYW")
    browser.find_element_by_name("password").send_keys("oiadklz")
    tf.global_variables_initializer()
    with tf.Session() as sess:
        tool.saver.restore(sess, tf.train.latest_checkpoint("./indexCliModel"))
        print "checkpoint:", tf.train.latest_checkpoint("./indexCliModel")
        predict = tf.argmax(tf.reshape(tool.output, [-1, MAX_CAPTCHA, CHAR_SET_LEN]), 2)
        # 初始化瀏覽器
        browser = tool.initBrowser(driver, url, secondurl=None)
        browser.save_screenshot(loginSnapshotPath)
        # 取得驗證碼
        location = browser.find_element_by_id("captchaimglogin").location
        print "location:", location
        x = location['x']
        y = location['y']
        image = tool.get_captcha(loginSnapshotPath, (x, y, x + 149, y + 33))
        if (mode == "debug"):
            image.show()
        validation_image = np.array(image) 
        captcha_image = convert2gray(validation_image)
        captcha_image = captcha_image.flatten() / 255
        text_list = sess.run(predict, feed_dict={X: [captcha_image], keep_prob: 1})
        text = text_list[0].tolist()  # 應該是機率最高的那一組
        vector = np.zeros(MAX_CAPTCHA * CHAR_SET_LEN)
        i = 0
        for n in text:
                vector[i * CHAR_SET_LEN + n] = 1  # ex:ABCDE vector[1*26+65] vector[2*26+66] vector[3*26+67] ...
                i += 1
        predict_text = vec2text(vector)
        print predict_text
        browser.find_element_by_name("captcha").send_keys(predict_text)
        browser.find_element_by_name("login").click()
    time.sleep(5);
    # 手動登入
    cookiefile = open("./cookies/test.json","wb")
    cookieString = ""
    cookies_dict = {}
    for cookie in browser.get_cookies():
        #     {u'domain': u'www.indexclix.com', u'name': u'loginset', u'expires': u'\u9031\u4e00, 08 1\u6708 2018 14:15:53 GMT', u'value': u'1502892678', u'expiry': 1515420953, u'path': u'/', u'httponly': False, u'secure': False}
# {u'domain': u'www.indexclix.com', u'name': u'login', u'value': u'submit', u'path': u'/', u'httponly': False, u'secure': False}
# {u'domain': u'.indexclix.com', u'name': u'__utmz', u'expires': u'\u9031\u56db, 15 2\u6708 2018 02:15:28 GMT', u'value': u'12841583.1502892928.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)', u'expiry': 1518660928, u'path': u'/', u'httponly': False, u'secure': False}
# {u'domain': u'.indexclix.com', u'name': u'__utmc', u'value': u'12841583', u'path': u'/', u'httponly': False, u'secure': False}
# {u'domain': u'.indexclix.com', u'name': u'__utmb', u'expires': u'\u9031\u4e09, 16 8\u6708 2017 14:45:28 GMT', u'value': u'12841583.1.10.1502892928', u'expiry': 1502894728, u'path': u'/', u'httponly': False, u'secure': False}
# {u'domain': u'.indexclix.com', u'name': u'__utma', u'expires': u'\u9031\u4e94, 16 8\u6708 2019 14:15:28 GMT', u'value': u'12841583.1789733411.1502892928.1502892928.1502892928.1', u'expiry': 1565964928, u'path': u'/', u'httponly': False, u'secure': False}
# {u'domain': u'.indexclix.com', u'name': u'__utmt', u'expires': u'\u9031\u4e09, 16 8\u6708 2017 14:25:28 GMT', u'value': u'1', u'expiry': 1502893528, u'path': u'/', u'httponly': False, u'secure': False}
# {u'domain': u'www.indexclix.com', u'name': u'PHPSESSID', u'value': u'73fc746974fca5c5e8b356ed1c97880b', u'path': u'/', u'httponly': False, u'secure': False}
# {u'domain': u'.indexclix.com', u'name': u'__cfduid', u'expires': u'\u9031\u56db, 16 8\u6708 2018 14:11:17 GMT', u'value': u'da1b82a745aef1ccfda3e28553ba679f71502892677', u'expiry': 1534428677, u'path': u'/', u'httponly': True, u'secure': False}
        json_cookie =  json.dumps(cookie.replace("u'","'"),ensure_ascii=False)
        
    cookiefile.write(cookieString)
    cookiefile.close()
#     cookies = open("./cookies/cookieGYPVYW.json","rb").read().replace("u","").replace("'",'"')
#     print cookies
#     json = json.loads(cookies)
#     print json['__tmz']
#     image = Image.open("./resource/test.jpg")
#     image.show()
#     start = {'x':5,'y':44}
#     end = {'x':400,'y':15}
#     image1 = indexClixFlow.cutImageValidation(image, "more", start, end, "debug")
