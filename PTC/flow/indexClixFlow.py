# coding=utf-8
'''
Created on 2017年8月12日

@author: master
'''
import Image
import datetime
import getpass
import logging
from random import randint
from selenium.common.exceptions import TimeoutException, UnexpectedAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
import sys
import time


sys.path.append("/home/"+getpass.getuser()+"/PAPS")



class flow():
    logger = logging.getLogger("indexClixFlow")
    browser = None
    windowhandle = None
    mode = None
    totalTimeSleep = 0
    step_log = 0
    cursor = None
    account = None
    def __init__(self,arg_c,arg_account,arg_browser,arg_windowhandle,validationSnapshotPath,than,arg_mode):
        self.browser = arg_browser
        self.windowhandle = arg_windowhandle
        self.mode = arg_mode
        self.account = arg_account
        self.cursor = arg_c
        self.flow_validation_result()
        self.autoClick(validationSnapshotPath, than)
        self.browser.switch_to_window(self.windowhandle)
        self.timeSleep("任務結束,瀏覽器切換到主選單",5)
        print "----------------------totalTimeSleep:",self.totalTimeSleep,"--------------------"
        self.flow_validation_result()
        
    def updateMoney(self):
#         browser.get("http://www.indexclix.com/index.php?view=account")
#         time.sleep(10)
#         money = browser.find_elements_by_tag_name("")[1].text
        connection = self.cursor.cursor()
#         print self.browser.page_source
        print self.browser.find_element_by_xpath("//li[@title='Account balance']").get_attribute('innerHTML')
        print self.browser.find_element_by_xpath("//li[@title='Account balance']").find_element_by_tag_name("a").get_attribute('innerHTML')
        try:
            money = self.browser.find_element_by_xpath("//li[@title='Account balance']").find_element_by_tag_name("a").text
        except:
            print "money is space"
            money = self.browser.find_element_by_xpath("//a[@href='/?view=account']")[0].text
        print "money:",money
        if not money is None and not money is "":
            connection.execute("UPDATE USERINFO SET MONEY = ?,UPDATETIME = ? WHERE USERNAME=?",(money,datetime.datetime.now(),self.account))
            self.cursor.commit()
        else:
            print "get Money Error"
        
    def timeSleep(self,why,count):
        print "    sleep because:",why
        for c in range(0,count):
            time.sleep(1)
            self.totalTimeSleep += 1
            print "  sleep:",c+1,"/",count,
        print "<<sleep finish>>"
    def flow_validation_result(self):
        print "flow5 : result"
        adblock_disabled_len = 0
        adblock_len = len(self.browser.find_elements_by_class_name("ad-block"))
        for adblock in self.browser.find_elements_by_class_name("ad-block"):
            if "disabled" in adblock.get_attribute("class"):
                adblock_disabled_len += 1
        print "adblock_disabled_len:",adblock_disabled_len,"adblock_len:",adblock_len
        if(adblock_disabled_len == adblock_len):
            if (adblock_disabled_len !=0 and adblock_len != 0):
                self.browser.quit()
                print "finish!"
            return True
    def switch_to_main_window(self):
        windowSize = len(self.browser.window_handles)
        if (windowSize >= 2):
            print "視窗過多關閉除第一個視窗"
            for window in self.browser.window_handles:
                if window != self.windowhandle:
                    self.browser.switch_to_window(window)
                    self.browser.close()
                else:
                    self.browser.switch_to_window(self.browser.window_handles[0])
                
    def switch_to_sub_windows(self):
        for window in self.browser.window_handles:
                if(window == self.windowhandle):
                    continue
                self.browser.switch_to_window(window)
    def autoClick(self,validationSnapshotPath, than):
            self.AD_init()
            ignoreList = []
            flowFlag = 1
            self.updateMoney()
            for step in range(0, len(self.browser.find_elements_by_class_name("ad-block"))):
                try:
                    flowFlag = 1
                    self.step_log = step
                    ignoreList = self.AD_ignoreList()
                    self.switch_to_main_window()
                    className = self.browser.find_elements_by_class_name("ad-block")[step].get_attribute("class")
                    if "disabled" in className:
                        continue
                    if step in ignoreList:
                        print step,"in the ignore list"
                        continue
                    print  "className:",className,"Step", step,"ad-block.length:",len(self.browser.find_elements_by_class_name("ad-block"))
                    self.browser.save_screenshot("./resource/orderStatus.jpg")
                    print "save",step ," flow snapshot ","./resource/orderStatus.jpg"
                    flowFlag = 2
                    self.switch_to_main_window()
                    if (not self.AD_click(step)):
                        self.logger.warning("auto click fail")
                        self.switch_to_main_window()
                        continue
                    self.timeSleep("點擊開啟廣告頁面",randint(5,10))
                    flowFlag = 3
                    self.switch_to_sub_windows()
                    validation_count = 0
                    while ( not self.AD_vaildation_loop(validationSnapshotPath)):
                        self.logger.warning("auto validation image fail")
                        validation_count +=1
                        if validation_count >=5:
                            self.timeSleep("驗證flow3超過5次by pass", 5)
                            break
#                         self.switch_to_main_window()
#                         break
                    self.timeSleep("完成驗證圖片後使用大量save_screenSnaphot降低資源",randint(5,10))
                    flowFlag = 4
                    self.switch_to_sub_windows()
                    if not self.AD_validation_click(validationSnapshotPath, than):
                        self.logger.warning("auto click validation image")
                        self.switch_to_main_window()
                        continue
                    flowFlag = 5
                    self.timeSleep("點擊驗證圖片後等確認再關閉",10)
                    for window in self.browser.window_handles:
                        self.browser.switch_to_window(window)
                    self.browser.close()
                    for window in self.browser.window_handles:
                        self.browser.switch_to_window(window)
                    self.windowhandle = self.browser.current_window_handle
                    print "windowhandle:",self.windowhandle
                    self.switch_to_main_window()
                    self.timeSleep("關閉視窗後休息",randint(5,10))
                    self.updateMoney()
                except:
                    self.AD_error_handle(flowFlag)
                    self.AD_addignroe()
                    raise
    # 驗證廣告圖片
    def cutImageValidation(self, image, than, location, endlocation):
        imagelist = []
        location_x = location['x'] + 25
        end_y = endlocation['y']
        location_y = location['y'] / 2 + end_y
        for _i in range(0, 6):
            imagelist.append(image.crop((location_x, end_y, location_x + 25, location_y)).convert("1"))
            if(self.mode == "debug"):
                image.crop((location_x, end_y, location_x + 25, location_y)).show()
            print "x1:", location_x, "y1:", end_y, "x2:", location_x + 25, "y2:", location_y
            location_x = location_x + 50
        less_block_position = 0
        less_black = imagelist[0]
        more_block_position = 0
        more_black = 0
        for step, im in enumerate(imagelist):
            width, height = im.size
            block_point = 0
            for w in range(0, width):
                for h in range(0, height):
                    if(im.getpixel((w, h)) == 255):
                        block_point += 1
            if(less_black > block_point):
                less_black = block_point
                less_block_position = step
            if(more_black < block_point):
                more_black = block_point
                more_block_position = step
        print "more_black:", more_black, "more_block_position:", more_block_position,
        print "less_black:",less_black,"less_block_position:",less_block_position
        if(than == "less"):
            return less_block_position
        elif(than == "more"):
            return more_block_position
        else:
            return less_block_position, more_block_position
        return -1
    #phantomjs 截圖前先用js處理
    def excute_javascript(self):                
        self.browser.execute_script("""$('.logo').append($("img[src='modules.php?m=surfer&show=captcha']"))""")
        self.browser.execute_script("""$("img[src='modules.php?m=surfer&show=captcha']").append($('td')[0])""")
        print "javascript execute is success!"
    #進入頁面重新refresh            
    def AD_init(self):
        print "flow1: init"
        for window in self.browser.window_handles:
                if(window == self.windowhandle):
                    continue
                self.browser.switch_to_window(window)
                self.browser.close()
        self.browser.switch_to_window(self.windowhandle)
        self.timeSleep("切換到主目錄等待",5)
        try:
            self.browser.find_element_by_id("dropdown-toggle").click()
        except:
            pass
        self.timeSleep("確認沒有彈出alert",randint(5,10))
#         try:
#             self.browser.find_element_by_link_text("View Advertisements").click()
#         except:
        print "取得廣告目錄中 . . ."
        try:
            self.browser.get("http://www.indexclix.com/index.php?view=ads")
        except TimeoutException:
            self.browser.get("http://www.indexclix.com/index.php?view=ads")
        self.timeSleep("取得廣告目錄完成休息,等待頁面初始化",10)
    def click_alert(self):
        try:
            for w in self.browser.window_handles:
                    self.browser.switch_to_window(w)
                    try:
                        self.browser.switch_to.alert.accept()
                        self.browser.find_elements_by_tag_name("body").send_keys(Keys.ESCAPE)
                    except:
                        print "no such alert"
        except:
            raise   
    #點擊廣告頁面
    def AD_click(self,step):
        print "flow2:auto click"
        try:
            try:
                self.browser.find_elements_by_class_name("pointer")[step].click()
            except:
                pass
            self.browser.set_window_size(1920,1080)
            self.timeSleep("開啟廣告頁面",20)
            
            self.click_alert()
            element = WebDriverWait(self.browser, 100).until(
                expected_conditions.presence_of_element_located((By.TAG_NAME, "body"))
            )
            self.click_alert()
            for index,window in enumerate(self.browser.window_handles):
                if window== self.windowhandle:
                        continue
                print "index:",index," window:",window," title:",self.browser.title
                self.browser.switch_to_window(window)
           
            current = True
            self.timeSleep("準備檢查", 5)
            self.click_alert()
            windowcount = len(self.browser.window_handles)
            if windowcount < 2:
                print "開啟的視窗數量少於2 Window Count:",windowcount
                self.timeSleep("準備結束...", 5)
                return False
            self.browser.set_window_size(1920,200)
            count = 0
            while(current):
                self.click_alert()
                self.switch_to_sub_windows()
                self.browser.save_screenshot("./resource/test.jpg")
                try:
                    location = self.browser.find_element_by_id("vnumbers").location
                except UnexpectedAlertPresentException:
                    self.timeSleep("Alert Text:None",5)
                    current = True
                    continue
                location_x = location['x']
                location_y = location['y']
                print "vnumbers location:",location
                if  (location_x != 0):
                    self.timeSleep("已變化 等待完成", 5)
                    self.browser.set_window_size(1920,1080)
                    current = False
                elif count > 30:
                    self.timeSleep("太久無法找到更新，try by pass", 1)
                    self.browser.set_window_size(1920,1080)
                    current = False
                else:
                    self.timeSleep("沒變化 adwait驗證中...", 10)
                    current = True
                    count += 1
        except:
            raise
        return True
    #如果有廣告則點擊
    def AD_try_alert(self):
        try:
            print "flow2.5: try click alert window"
            for index,handle in enumerate(self.browser.window_handles):
                if handle== self.windowhandle:
                    continue
                print "index:",index," window:",handle," title:",self.browser.title
                self.browser.switch_to.window(handle)
            self.browser.switch_to.alert.accept()
            self.browser.find_elements_by_tag_name("body").send_keys(Keys.ESCAPE)
            print "self.browser.find_elements_by_tag_name('body').send_keys()"
        except:
            alert = self.browser.switch_to.alert
            alert.accept()
            print "alert.accept()"
            pass
    #驗證是否可以點擊    
    def AD_vaildation_loop(self,validationSnapshotPath):
        print "flow3: validation image"
        try:
            self.switch_to_sub_windows()
            try:
                self.excute_javascript()
            except:
                self.logger.warning("廣告網頁有bug，無法初始化略過此廣告")
                return False
            self.timeSleep("等待驗證圖片重新排版完成",1)
            self.browser.set_window_size(1920,200)
            self.timeSleep("變更視窗大小等待視窗變化完成",3)
            self.browser.save_screenshot(validationSnapshotPath)
            self.timeSleep("拍照完成釋放資源", 5)
            print "save:",validationSnapshotPath
            self.browser.set_window_size(1920,1080)
            try:
                testimage = Image.open(validationSnapshotPath)
            except IOError:
                try:
                    testimage.close()
                except:
                    print "image close error return False"
                    return False
                print "image open error return False"
                return False
                
            size = {'x':5,'y':44}
            area = {'x':300,'y':15}
            less_position, more_position = self.cutImageValidation(testimage, "", size, area)
            self.timeSleep("計算圖片完成等待釋放資源", 5)
            print "less_position:",less_position,"more_position:",more_position
            if less_position == more_position :
                return False
            else:
                return True
        except:
            try:
                if "You already visited this advertisement in the last 24 hours" in self.browser.find_element_by_class_name("errorbox").text:
                    return False
    #             elif "" == self.browser.find_element_by_class_name("errorbox").text:
    #                 return True
                else:
                    return True
            except:
                pass
            raise
    #點擊驗證圖案    
    def AD_validation_click(self,validationSnapshotPath,than):
        print "flow4: auto click validation image"
        try:
            try:
                testimage = Image.open(validationSnapshotPath)
                size = {'x':5,'y':44}
                area = {'x':400,'y':15}
                print "size:", size,"area:", area
                position = self.cutImageValidation(testimage, than, size, area)
                self.browser.set_window_size(1920,200)
                self.browser.save_screenshot("./resource/status.jpg")
                print "save:","./resource/status.jpg"
                self.timeSleep("等待頁面儲存",randint(5,10))
                self.browser.set_window_size(1920,1080)
                self.browser.execute_script("endprogress(" + str(position + 1) + ");")
            except:
                pass
            try:
                self.timeSleep("等待頁面加載完成",15)
                result = self.browser.find_element_by_class_name("successbox").text    
                print "驗證結果：",result
                if "Thanks for watching!" in result:
                    return True
                else:
                    return False
            except:
                #已經驗證完成有沒有判斷成功沒那麼重要
                return True
        except:
            raise
    def AD_error_handle(self,flowFlag):
        self.AD_addignroe()
        try:
            print "錯誤在flow",flowFlag
            windowSize = len(self.browser.window_handles)
            if(windowSize < 2):
                print "window數量小於2，無法判斷是否為主要視窗"
            else:
                print "發生錯誤"
                for window in self.browser.window_handles:
                    print "window:",window
                    if window != self.windowhandle:
                        self.browser.switch_to_window(window)
                        self.browser.close()
                self.browser.switch_to_window(self.browser.window_handles[0])
                self.windowhandle = self.browser.current_window_handle
        except Exception as e:
            print str(e)
    #加入忽略清單
    def AD_addignroe(self):
        userStep = open("./resource/step","ab")
        userStep.write(str(self.step_log)+"\n")
        self.timeSleep("程式錯誤加入到忽略清單中", 5)
        userStep.close()
    #回傳忽略的步驟
    def AD_ignoreList(self):
        ignoreList = []
        userStep = open("./resource/step","rb")
        for line in userStep.readlines():
            ignoreList.append(int(line))
            print "ignore:",line
        userStep.close()
        return ignoreList
    
    
