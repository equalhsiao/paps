#coding=utf-8
'''
Created on 2017年7月3日

@author: master
'''


from PTC.gen_captcha import ALPHABET
from PTC.gen_captcha import gen_captcha_text_and_image
import os
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

text, image = gen_captcha_text_and_image(0)
print("验证码图像channel:", image.shape)  # (60, 160, 3)
# 图像大小
IMAGE_HEIGHT = 40
IMAGE_WIDTH = 180
MAX_CAPTCHA = len(text)
print "MAX_CAPTCH:"+str(MAX_CAPTCHA)
print("验证码文本最长字符数", MAX_CAPTCHA)   # 验证码最长4字符; 我全部固定为4,可以不固定. 如果验证码长度小于4，用'_'补齐

# 把彩色图像转为灰度图像（色彩对识别验证码没有什么用）
def convert2gray(img):
    if len(img.shape) > 2:
        gray = np.mean(img, -1)
        plt.show(gray)
        # 上面的转法较快，正规转法如下
        # r, g, b = img[:,:,0], img[:,:,1], img[:,:,2]
        # gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
        return gray
    else:
        return img

"""
cnn在图像大小是2的倍数时性能最高, 如果你用的图像大小不是2的倍数，可以在图像边缘补无用像素。
np.pad(image,((2,3),(2,2)), 'constant', constant_values=(255,))  # 在图像上补2行，下补3行，左补2行，右补2行
"""

# 文本转向量
# char_set = number + alphabet + ALPHABET + ['_']  # 如果验证码长度小于4, '_'用来补齐
char_set = ALPHABET 
CHAR_SET_LEN = len(char_set)
print "CAHR_SET_LEN:"+str(CHAR_SET_LEN)
def text2vec(text):
    text_len = len(text)
    if text_len > MAX_CAPTCHA:
        print "length error:",text
        raise ValueError('验证码最长5个字符')

    vector = np.zeros(MAX_CAPTCHA*CHAR_SET_LEN) #130的矩陣[ 0 0 0 ... 0 0 0]
#     print vector.shape #(130,)
    def char2pos(c):
#         print "c:"+c
#         print "ord(c):"+str(ord(c))
        if c =='_':
            k = 26      #若是為'_'則返回大寫字母A的位置
            return k
        k = ord(c)-65   #若是A則會再第1個位置   
        if k > 26:      #若k超過A的charCode
            k = ord(c) - 55 #第10個位置
            if k > 35:
                k = ord(c) - 61
                if k > 61:
                    raise ValueError('No Map') 
        return k
    for i, c in enumerate(text):
        idx = i * CHAR_SET_LEN + char2pos(c) #計算特徵 5*26+ charcode
        vector[idx] = 1
    return vector
# 向量转回文本
def vec2text(vec):
    char_pos = vec.nonzero()[0]
    text=[]
    for i, c in enumerate(char_pos):
        char_at_pos = i #c/63
        char_idx = c % CHAR_SET_LEN
        char_code=char_idx+65
#         if char_idx <36:
#             char_code = char_idx - 10 + ord('A')
#         elif char_idx < 62:
#             char_code = char_idx-  36 + ord('a')
#         elif char_idx == 62:
#             char_code = ord('_')
#         else:
#             raise ValueError('error')
        text.append(chr(char_code))
    return "".join(text)

"""
#向量（大小MAX_CAPTCHA*CHAR_SET_LEN）用0,1编码 每63个编码一个字符，这样顺利有，字符也有
vec = text2vec("F5Sd")
text = vec2text(vec)
print(text)  # F5Sd
vec = text2vec("SFd5")
text = vec2text(vec)
print(text)  # SFd5
"""

# 生成一个训练batch
def get_next_batch(train_type,batch_size=128):
    batch_x = np.zeros([batch_size, IMAGE_HEIGHT*IMAGE_WIDTH])
    batch_y = np.zeros([batch_size, MAX_CAPTCHA*CHAR_SET_LEN])
    
    # 有时生成图像大小不是(60, 160, 3)
    def wrap_gen_captcha_text_and_image():
        while True:
            text, image = gen_captcha_text_and_image(train_type)
            if image.shape == (40, 180, 3):
                return text, image

    for i in range(batch_size):
        text, image = wrap_gen_captcha_text_and_image()
        image = convert2gray(image)
        plt.show(image)

        batch_x[i,:] = image.flatten() / 255 # (image.flatten()-128)/128  mean为0
        batch_y[i,:] = text2vec(text)

    return batch_x, batch_y

####################################################################
# with tf.name_scope('inputs'):
X = tf.placeholder(tf.float32, [None, IMAGE_HEIGHT*IMAGE_WIDTH])
print "Y:"+str(MAX_CAPTCHA*CHAR_SET_LEN)
Y = tf.placeholder(tf.float32, [None, MAX_CAPTCHA*CHAR_SET_LEN])
keep_prob = tf.placeholder(tf.float32) # dropout

# 定义CNN
def crack_captcha_cnn(w_alpha=0.01, b_alpha=0.1):
    x = tf.reshape(X, shape=[-1, IMAGE_HEIGHT, IMAGE_WIDTH, 1])
    #w_c1_alpha = np.sqrt(2.0/(IMAGE_HEIGHT*IMAGE_WIDTH)) #
    #w_c2_alpha = np.sqrt(2.0/(3*3*32)) 
    #w_c3_alpha = np.sqrt(2.0/(3*3*64)) 
    #w_d1_alpha = np.sqrt(2.0/(8*32*64))
    #out_alpha = np.sqrt(2.0/1024)
    
    # 3 conv layer
    with tf.name_scope("layers"):
        with tf.name_scope("layer1"):
            with tf.name_scope("w_c1"):
                w_c1 = tf.Variable(w_alpha*tf.random_normal([3, 3, 1, 32]))
                tf.summary.histogram("w_c1", w_c1)
            with tf.name_scope("b_c1"):
                b_c1 = tf.Variable(b_alpha*tf.random_normal([32]))
                tf.summary.histogram("b_c1", b_c1)
    conv1 = tf.nn.relu(tf.nn.bias_add(tf.nn.conv2d(x, w_c1, strides=[1, 1, 1, 1], padding='SAME'), b_c1))
    conv1 = tf.nn.max_pool(conv1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
    conv1 = tf.nn.dropout(conv1, keep_prob)
    with tf.name_scope("layer2"):
        with tf.name_scope("w_c2"):
            w_c2 = tf.Variable(w_alpha*tf.random_normal([3, 3, 32, 64]))
            tf.summary.histogram("w_c2", w_c2)
        with tf.name_scope("b_c2"):
            b_c2 = tf.Variable(b_alpha*tf.random_normal([64]))
            tf.summary.histogram("b_c2", b_c2)
    conv2 = tf.nn.relu(tf.nn.bias_add(tf.nn.conv2d(conv1, w_c2, strides=[1, 1, 1, 1], padding='SAME'), b_c2))
    conv2 = tf.nn.max_pool(conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
    conv2 = tf.nn.dropout(conv2, keep_prob)
    with tf.name_scope("layer3"):
        with tf.name_scope("w_c3"):
            w_c3 = tf.Variable(w_alpha*tf.random_normal([3, 3, 64, 64]))
            tf.summary.histogram("w_c3", w_c3)
        with tf.name_scope("b_c3"):
            b_c3 = tf.Variable(b_alpha*tf.random_normal([64]))
            tf.summary.histogram("b_c3", b_c3)
    conv3 = tf.nn.relu(tf.nn.bias_add(tf.nn.conv2d(conv2, w_c3, strides=[1, 1, 1, 1], padding='SAME'), b_c3))
    conv3 = tf.nn.max_pool(conv3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
    conv3 = tf.nn.dropout(conv3, keep_prob)
        
        # Fully connected layer
    w_d = tf.Variable(w_alpha*tf.random_normal([8*20*46, 1024]))
    tf.summary.histogram("Fully_layer/w_d", w_d)
    b_d = tf.Variable(b_alpha*tf.random_normal([1024]))
    tf.summary.histogram("Fully_layer/b_d", b_d)
    dense = tf.reshape(conv3, [-1, w_d.get_shape().as_list()[0]])
    dense = tf.nn.relu(tf.add(tf.matmul(dense, w_d), b_d))
    dense = tf.nn.dropout(dense, keep_prob)
    
    w_out = tf.Variable(w_alpha*tf.random_normal([1024, MAX_CAPTCHA*CHAR_SET_LEN]))
    tf.summary.histogram("Fully_layer/w_out", w_out)
    b_out = tf.Variable(b_alpha*tf.random_normal([MAX_CAPTCHA*CHAR_SET_LEN]))
    tf.summary.histogram("Fully_layer/b_out", b_out)
    out = tf.add(tf.matmul(dense, w_out), b_out)
    tf.summary.histogram("Fully_layer/out", out)
        #out = tf.nn.softmax(out)
#     if use_type == "use":
#         saver = tf.train.Saver({"w1":w_c1,'b1':b_c1,'w2':w_c2,'b2':b_c2,'w3':w_c3,'b3':b_c3,'wd':w_d,'bd':b_d,'wo':w_out,'bo':b_out})
#         return out,saver
    return out
def crack_captcha(captcha_image):
    output = crack_captcha_cnn()
    saver = tf.train.Saver()
    with tf.Session() as sess:
        saver.restore(sess, tf.train.latest_checkpoint('./model'))
        predict = tf.argmax(tf.reshape(output, [-1, MAX_CAPTCHA, CHAR_SET_LEN]), 2)
        text_list = sess.run(predict, feed_dict={X: [captcha_image], keep_prob: 1})
        text = text_list[0].tolist()  #應該是機率最高的那一組
            
        vector = np.zeros(MAX_CAPTCHA*CHAR_SET_LEN)
        i = 0
        for n in text:
                vector[i*CHAR_SET_LEN + n] = 1  #ex:ABCDE vector[1*26+65] vector[2*26+66] vector[3*26+67] ...
                i += 1
        return vec2text(vector)

# 训练
def train_crack_captcha_cnn(baseModelPath,saveModelPath):
    if not os.path.exists(saveModelPath):
        os.mkdir(saveModelPath)
        
    output = crack_captcha_cnn()
    # loss
    #loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(output, Y))
    print "output.shape:"+str(output.shape)
    with tf.name_scope("loss"):
        loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=output, labels=Y))
        tf.summary.scalar('loss',loss)
        # 最后一层用来分类的softmax和sigmoid有什么不同？
    # optimizer 为了加快训练 learning_rate应该开始大，然后慢慢衰
    optimizer = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)

    predict = tf.reshape(output, [-1, MAX_CAPTCHA, CHAR_SET_LEN])
    max_idx_p = tf.argmax(predict, 2)
    max_idx_l = tf.argmax(tf.reshape(Y, [-1, MAX_CAPTCHA, CHAR_SET_LEN]), 2)
    correct_pred = tf.equal(max_idx_p, max_idx_l)
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
    tf.summary.scalar('accuracy', accuracy)
    tf.summary.histogram('loss ', loss)
    saver = tf.train.Saver()
    merged = tf.summary.merge_all()
    step = 0
    temp_step = 0
    with tf.Session() as sess:
        if os.path.exists(baseModelPath+'/checkpoint'): #判断模型是否存在
            print "模型存在繼續訓練",tf.train.latest_checkpoint(baseModelPath)
            step = int(str(tf.train.latest_checkpoint(baseModelPath)).split("-")[1])
            temp_step = step
            saver.restore(sess, tf.train.latest_checkpoint(baseModelPath)) #存在就从模型中恢复变量
        else:
            print "模型不存在初始化中"
            init = tf.global_variables_initializer() #不存在就初始化变量
            sess.run(init)
        writer  = tf.summary.FileWriter('/tmp/tensorboard', sess.graph)
        while True:
#         for step in range(501):
            batch_x, batch_y = get_next_batch(2,8)
            _, loss_ = sess.run([optimizer, loss], feed_dict={X: batch_x, Y: batch_y, keep_prob: 0.75})
            print (step, loss_)
            tf.summary.scalar('loss', loss_)
            # 每100 step计算一次准确率
            if step % 30 == 0:
                batch_x_test, batch_y_test = get_next_batch(1,128)
                summary,acc = sess.run([merged,accuracy], feed_dict={X: batch_x_test, Y: batch_y_test, keep_prob: 1.})
                writer.add_summary(summary, step)
                print "訓練結果："
                print (step, acc)
#             if step % 500 == 0 and step != 0:
                # 如果准确率大于50%,保存模型,完成训练
#             if step % 30 == 0:
#                 saver.save(sess, "./model/crack_capcha.ckpt",step)
#             if acc > 0.99:
#                 saver.save(sess, saveModelPath + "/crack_capcha.ckpt",step)
#                 break
#             if  step % 30 == 0:
#                 saver.save(sess, "./model/crack_capcha.ckpt",step)
#                 break
            if step == temp_step+60:
                saver.save(sess, saveModelPath + "/crack_capcha.ckpt",step)
                break
            step += 1
if __name__ == '__main__':
    saveModelPath = "./indexCliModel"
    baseModelPath = "./indexCliModel"
#     baseModelPath = "./model"
    train_crack_captcha_cnn(baseModelPath,saveModelPath)
    
    
    