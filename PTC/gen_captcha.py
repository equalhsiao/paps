#coding=utf-8
'''
Created on 2017年7月3日

@author: master
'''
from PIL import Image
import os
from random import randint
import random

from PTC.mergePng import create_captcha
from PTC.optTool import cleanbackgroup
import matplotlib.pyplot as plt
import numpy as np


# 验证码中的字符, 就不用汉字了
# number = ['0','1','2','3','4','5','6','7','8','9']
# alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
ALPHABET = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
# 验证码一般都无视大小写；验证码长度4个字符 char_set=number+alphabet+ALPHABET
def random_captcha_text(char_set=ALPHABET, captcha_size=5):
    captcha_text = []
    for _i in range(captcha_size):
        c = random.choice(char_set)
        captcha_text.append(c)
    return captcha_text
 
# 生成字符对应的验证码
count_tmp = 0

DATA_DIR = "/home/master/images"
file_name = []
file_data = []
def gen_captcha_text_and_image(train_type):
#     image = ImageCaptcha()
#   
#     captcha_text = random_captcha_text()
#     captcha_text = ''.join(captcha_text)
#  
#     captcha = image.generate(captcha_text)
#     #image.write(captcha_text, captcha_text + '.jpg')  # 写到文件
#  
#     captcha_image = Image.open(captcha)
#     captcha_image = np.array(captcha_image)
    file_name = []
    if(train_type==0):
        captcha_text,captcha_image = create_captcha()
    elif(train_type==1):
        DATA_DIR = "/home/master/validation_images"
        for filename in os.listdir(DATA_DIR):
            file_name.append(filename)
        test_count = randint(0,len(file_name)-1 )
        captcha_image = Image.open(DATA_DIR+"/"+file_name[test_count])
        captcha_text = file_name[test_count].split(".")[0]
        width,height = captcha_image.size
        captcha_image = cleanbackgroup(width,height,captcha_image)
        captcha_image = np.array(captcha_image)
    else:
        DATA_DIR = "/home/master/modify_image"
        for filename in os.listdir(DATA_DIR):
            file_name.append(filename)
        test_count = randint(0,len(file_name)-1 )
        captcha_image = Image.open(DATA_DIR+"/"+file_name[test_count])
        captcha_text = file_name[test_count].split(".")[0]
        width,height = captcha_image.size
        captcha_image = cleanbackgroup(width,height,captcha_image)
        captcha_image = captcha_image.convert('RGB')
        captcha_image = np.array(captcha_image)
        
#         captcha_image.close()
#     count_tmp +=1
    
    return captcha_text, captcha_image
 
if __name__ == '__main__':
    # 测试
    for step in range(0,4,1):
        text, image = gen_captcha_text_and_image(1)
        f = plt.figure()
        ax = f.add_subplot(111)
        ax.text(0.1, 0.9,text, ha='center', va='center', transform=ax.transAxes)
        image.show()
        print text
    