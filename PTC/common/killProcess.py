# coding=utf-8
'''
Created on 2017年9月6日

@author: master
'''
import os
import sys
import getpass
sys.path.append("/home/"+getpass.getuser()+"/workspace/PAPS")

if __name__ == '__main__':
    import subprocess, signal
    p = subprocess.Popen(['ps', 'aux'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.splitlines():
        if 'filefox' in line:
            pid = int(line.split(None, 10)[1])
            print line
            os.kill(pid, signal.SIGKILL)